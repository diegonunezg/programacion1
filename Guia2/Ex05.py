"""
Ejercicio 5:

Escriba un programa que pida al usuario dos palabras, y se
indique cual de ellas es la mas larga y por cuantas letras lo es.

"""
# Se pide ingresar 2 palabras para compararlas.
palabra1 = input("Ingrese una palabra: ")
palabra2 = input("Ingrese otra palabra: ")

# Si alguna de las palabras contiene un numero, no se realiza la comparacion.
if not palabra1.isalpha() or not palabra2.isalpha():
    print("La(s) palabra(s) ingresada(s) contiene(n) caracteres numericos")
# En caso contrario, se calcula la cantidad de letras y se entrega el resultado.
else:
    len1 = len(palabra1)
    len2 = len(palabra2)

    if len1 == len2:
        print("Las palabras tienen la misma cantidad de letras")
    elif len1 > len2:
        print(f"La primera palabra es mas larga por {len1-len2} letras")
    else:
        print(f"La segunda palabra es mas larga por {len2-len1} letras")
