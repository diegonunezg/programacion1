"""
Ejercicio 4:

Extienda el programa anterior para logre encontrar palabras
dentro de un texto,sin importar si estan escritas en
diferente combinacion de mayusculas/minusculas.

"""

# Se ingresan tanto el texto como la palabra.
texto = input("Ingrese el texto a escanear: ")
palabra = input("Ingrese la palabra a escanear: ")

# Se convierte todo el texto a minusculas para buscar coincidencias.
texto = texto.lower()
palabra = palabra.lower()

# Se evaluan las condiciones y se entregan los resultados.
if texto == "" or palabra == "":
    print("Una de las entradas carece de contenido")
else:
    if palabra in texto:
        print(f"La palabra '{palabra}' se encuentra dentro del texto")
    else:
        print(f"La palabra '{palabra}' no se encuentra dentro del texto")
