"""
Ejercicio 7:

Problema de parecidos. Cree un programa que a partir de un ingreso de
5 palabras indique las dos palabras que mas se parecen y las dos que
menos se parecen. Una palabra es parecida cuando a lo menos el 70% de
sus caracteres aparecen en otra palabra, caso contrario NO HAY PARECIDO.

"""

# Se pide ingresar 5 palabras. En cada instancia se controla que se ingresen
# entradas validas. Tambien se dejan las palabras en minuscula por defecto.
while True:
    palabra1 = input("\nIngrese la primera palabra: ")
    if palabra1.isalpha():
        break
    else:
        print("\nLa palabra ingresada no es valida.")
        print("Solo se aceptan caracteres alfabeticos.")
palabra1 = palabra1.lower()

while True:
    palabra2 = input("\nIngrese la segunda palabra: ")
    if palabra2.isalpha():
        break
    else:
        print("\nLa palabra ingresada no es valida.")
        print("Solo se aceptan caracteres alfabeticos.")
palabra2 = palabra2.lower()

while True:
    palabra3 = input("\nIngrese la tercera palabra: ")
    if palabra3.isalpha():
        break
    else:
        print("\nLa palabra ingresada no es valida.")
        print("Solo se aceptan caracteres alfabeticos.")
palabra3 = palabra3.lower()

while True:
    palabra4 = input("\nIngrese la cuarta palabra: ")
    if palabra4.isalpha():
        break
    else:
        print("\nLa palabra ingresada no es valida.")
        print("Solo se aceptan caracteres alfabeticos.")
palabra4 = palabra4.lower()

while True:
    palabra5 = input("\nIngrese la quinta palabra: ")
    if palabra5.isalpha():
        break
    else:
        print("\nLa palabra ingresada no es valida.")
        print("Solo se aceptan caracteres alfabeticos.")
palabra5 = palabra5.lower()


"""
-------------------------------------------------------------------------------
Todas las palabras deben ser comparadas entre ellas a pares.
Dado este caso, el resultado de la combinatoria es 10, y se calcula:

             5          5!
            C   =  -----------  = 10
             2       2! * 3!

Tambien hay que tener en cuenta que dependiendo de cual palabra se compare
con otra, el porcentaje cambia. Por ejemplo:

    Palabra 1: hola
    Palabra 2: ola

Si comparamos la cantidad de letras de la primera palabra que se
encuentran en la segunda, tenemos que: 3/4 = 75%

Pero si comparamos la cantidad de letras de la segunda palabra
existentes en la primera obtenemos: 3/3 = 100%

Es por esto que al momento de hacer el calculo, se tomara como divisor
la cantidad mayor de letras entre las 2 palabras. En este ejemplo, seria 4.
Asi el porcentaje no variara segun que palabra se compare con cual.
-------------------------------------------------------------------------------

"""

# Comparacion de las palabras 1 y 2.
contador = 0
for letra in palabra1:
    if letra in palabra2:
        contador += 1
if len(palabra1) > len(palabra2):
    par12 = contador / float(len(palabra1))
else:
    par12 = contador / float(len(palabra2))

# Comparacion de las palabras 1 y 3.
contador = 0
for letra in palabra1:
    if letra in palabra3:
        contador += 1
if len(palabra1) > len(palabra3):
    par13 = contador / float(len(palabra1))
else:
    par13 = contador / float(len(palabra3))

# Comparacion de las palabras 1 y 4.
contador = 0
for letra in palabra1:
    if letra in palabra4:
        contador += 1
if len(palabra1) > len(palabra4):
    par14 = contador / float(len(palabra1))
else:
    par14 = contador / float(len(palabra4))

# Comparacion de las palabras 1 y 5.
contador = 0
for letra in palabra1:
    if letra in palabra5:
        contador += 1
if len(palabra1) > len(palabra5):
    par15 = contador / float(len(palabra1))
else:
    par15 = contador / float(len(palabra5))

# Comparacion de las palabras 2 y 3.
contador = 0
for letra in palabra2:
    if letra in palabra3:
        contador += 1
if len(palabra2) > len(palabra3):
    par23 = contador / float(len(palabra2))
else:
    par23 = contador / float(len(palabra3))

# Comparacion de las palabras 2 y 4.
contador = 0
for letra in palabra2:
    if letra in palabra4:
        contador += 1
if len(palabra2) > len(palabra4):
    par24 = contador / float(len(palabra2))
else:
    par24 = contador / float(len(palabra4))

# Comparacion de las palabras 2 y 5.
contador = 0
for letra in palabra2:
    if letra in palabra5:
        contador += 1
if len(palabra2) > len(palabra5):
    par25 = contador / float(len(palabra2))
else:
    par25 = contador / float(len(palabra5))

# Comparacion de las palabras 3 y 4.
contador = 0
for letra in palabra3:
    if letra in palabra4:
        contador += 1
if len(palabra3) > len(palabra4):
    par34 = contador / float(len(palabra3))
else:
    par34 = contador / float(len(palabra4))

# Comparacion de las palabras 3 y 5.
contador = 0
for letra in palabra3:
    if letra in palabra5:
        contador += 1
if len(palabra3) > len(palabra5):
    par35 = contador / float(len(palabra3))
else:
    par35 = contador / float(len(palabra5))

# Comparacion de las palabras 4 y 5.
contador = 0
for letra in palabra4:
    if letra in palabra5:
        contador += 1
if len(palabra4) > len(palabra5):
    par45 = contador / float(len(palabra4))
else:
    par45 = contador / float(len(palabra5))

# Se crea una lista con cada porcentaje de coincidencia por par.
parejas = [par12, par13, par14, par15, par23, par24, par25, par34, par35, par45]

# Con estas variables y un ciclo for, se buscaran el mayor y el menor porcentaje.
maximo = 0.0
minimo = 1.0
indicemax = None
indicemin = None

for indice in range(0, len(parejas)):
    if parejas[indice] > maximo:
        maximo = parejas[indice]
        indicemax = indice
    if parejas[indice] < minimo:
        minimo = parejas[indice]
        indicemin = indice

# Finalmente, segun cual haya sido el mayor %, se entregan los resultados.
print("\nLas palabras mas parecidas son :")
if indicemax == 0:
    print(f"{palabra1} y {palabra2}")
elif indicemax == 1:
    print(f"{palabra1} y {palabra3}")
elif indicemax == 2:
    print(f"{palabra1} y {palabra4}")
elif indicemax == 3:
    print(f"{palabra1} y {palabra5}")
elif indicemax == 4:
    print(f"{palabra2} y {palabra3}")
elif indicemax == 5:
    print(f"{palabra2} y {palabra4}")
elif indicemax == 6:
    print(f"{palabra2} y {palabra5}")
elif indicemax == 7:
    print(f"{palabra3} y {palabra4}")
elif indicemax == 8:
    print(f"{palabra3} y {palabra5}")
elif indicemax == 9:
    print(f"{palabra4} y {palabra5}")

# Finalmente, segun cual haya sido el menor %, se entregan los resultados.
print("\nLas palabras menos parecidas son :")
if indicemin == 0:
    print(f"{palabra1} y {palabra2}")
elif indicemin == 1:
    print(f"{palabra1} y {palabra3}")
elif indicemin == 2:
    print(f"{palabra1} y {palabra4}")
elif indicemin == 3:
    print(f"{palabra1} y {palabra5}")
elif indicemin == 4:
    print(f"{palabra2} y {palabra3}")
elif indicemin == 5:
    print(f"{palabra2} y {palabra4}")
elif indicemin == 6:
    print(f"{palabra2} y {palabra5}")
elif indicemin == 7:
    print(f"{palabra3} y {palabra4}")
elif indicemin == 8:
    print(f"{palabra3} y {palabra5}")
elif indicemin == 9:
    print(f"{palabra4} y {palabra5}")
