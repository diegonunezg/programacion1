""""
Ejercicio 3:

Escriba un programa que indique si una palabra existe dentro de un texto.
La palabra como la oracion seran ingresadas por el usuario.

"""

# Se ingresan tanto el texto como la palabra.
texto = input("Ingrese el texto a escanear: ")
palabra = input("Ingrese la palabra a escanear: ")

# Se evaluan las condiciones y se entregan los resultados.
if texto == "" or palabra == "":
    print("Una de las entradas carece de contenido")
else:
    if palabra in texto:
        print(f"La palabra '{palabra}' se encuentra dentro del texto")
    else:
        print(f"La palabra '{palabra}' no se encuentra dentro del texto")
