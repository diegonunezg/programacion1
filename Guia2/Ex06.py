"""
Ejercicio 6:

Cree un programa que permita concatenar los datos o palabras ingresadas
por un usuario. El ingreso de palabras debe estar controlado por un ciclo
y cuando se escriba la palabra fin termine la ejecucion
y se muestren todos los datos ingresados de forma concatenada.

"""

# Variables que guardardan las palabras a concatenar.
texto = ""
last_word = ""

# Ciclo que itera hasta que se ingrese la palabra "fin".
while last_word != "fin":
    palabra = input("Ingrese un dato o una palabra: ")
    texto += palabra
    last_word = palabra.lower()

# Finalmente se imprime el texto.
print(texto)
