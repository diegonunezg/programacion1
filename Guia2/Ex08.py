"""
Ejercicio 8:

Realizar el juego del ahorcado.
Primero se debe ingresar la palabra a adivinar de hasta 10 caracteres.
Luego se muestra por cada letra un guion bajo para que el jugador sepa
la cantidad de letras a adivinar. Se ir´a ingresando una a una las letras
y si estas se encuentran en la palabra las debera ir mostrando en el lugar
correspondiente. Por cada letra que no se encuentre en la palabra perdera
una vida. El jugador dispondra de 5 vidas para intentar ganar el juego.

"""
# Se importa random para añadir palabras aleatorias al juego.
import random

# Una lista de palabras que pueden ser escogidas. Se pueden añadir mas si se requiere.
palabras = [
           "estrella",
           "instructor",
           "elefante",
           "piton",
           "camara",
           "programar",
           "desafio",
           "excepcion",
           "pizarra",
           "palabra",
           "proteinas",
           "terminal"
           ]

print("\n\nBienvenido al juego del ahorcado\n")

# Se le da a elegir al usuario si quiere ingresar una palabra o si desea jugar con una predeterminada.
opcion = 0
while opcion != 1 and opcion != 2:
    print("\t< 1 > para jugar con una palabra aleatoria")
    print("\t< 2 > para ingresar una palabra manualmente")
    # Control de excepcion en caso de que no se ingrese un tipo de dato entero.
    try:
        opcion = int(input("\nIngrese una opcion: "))
    except ValueError:
        print("El texto ingresado no es numerico.")

# Si se escoge la opcion 1 la palabra es escogida al azar.
if opcion == 1:
    manual = random.choice(palabras)
# Sino, el usuario la ingresa.
else:
    while True:
        manual = input("Ingrese la palabra deseada (Maximo 10 letras): ")
        # Se verifica que se ingresen solo caracteres alfabeticos.
        if not manual.isalpha():
            print("\nLa palabra solo admite letras.\n")
            continue
        # Se verifica que la palabra ingresada tenga como maximo 10 letras
        elif len(manual) > 10:
            print("\nLa palabra ingresada posee mas de 10 letras.\n")
            continue
        else:
            manual = manual.lower()
            break

# Se crean 2 listas, una con las letras de la palabra y otra con espacios para ser rellenados.
palabra = []
adivinar = []
for letter in manual:
    palabra.append(letter)
    adivinar.append("_")

# Variables para almacenar la cantidad de vidas y las letras ya ingresadas.
vidas = 5
usadas = []

# Este ciclo le da la vida al juego. Y no se termina hasta que se acaben las vidas o se gane la partida.
while vidas > 0:
    # Con cada iteracion se hace reset a estas variables.
    imprimir = ""
    contador = 0

    # Se muestra informacion del juego en pantalla.
    print(f"\n\nVidas restantes {vidas}\nLetras usadas: {usadas}", )

    # Este ciclo genera el texto que ha sido adivinado hasta el momento.
    for espacio in range(0, len(manual)):
        imprimir += adivinar[espacio] + " "
    print(imprimir)

    # Se pide ingresar una letra.
    print("\n(Importante: Si ingresas algo que no es una letra o si \ningresas mas de una, seras penalizado con una vida.)")
    letra = input("\nIngresa una letra: ")

    # Si se ingreso algo de mas de 1 caracter o algo que no sea una letra, se resta una vida.
    if len(letra) == 1 and letra.isalpha():
        # Si el usuario ingresa una letra ya ingresada previamente no se le penaliza.
        if letra in usadas:
            print("\nYa has usado esta letra.")
            continue
        # Sino, se agrega a la lista de letras usadas.
        else:
            usadas.append(letra)
    # Si el usuario no cumplio con la instruccion pedida se le quita una vida.
    else:
        vidas -= 1
        continue

    # Se convierte la entrada del usuario a minusculas.
    letra = letra.lower()

    # Se revisa si la letra se encuentra en la palabra a adivinar.
    for indice in range(0, len(manual)):
        if letra == palabra[indice]:
            adivinar[indice] = letra
            contador += 1
    # De lo contrario, se resta una vida.
    if contador == 0:
        vidas -= 1

    # Si ya se adivino la palabra completa, el juego finaliza.
    if palabra == adivinar:
        print(f"\n{manual}")
        print("\nFelicidades, ha gando el juego.\n\n")
        break
    # Si el jugador se queda sin vidas, la partida finaliza. (cuando vidas == 0, se escapa del ciclo)
    if vidas == 0:
        print(f"\n\nHas perdido. La palabra era: {manual}\n")
