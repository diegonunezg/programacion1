"""
Ejercicio 2:

El siguiente String esta encriptado de mala manera a pesar que de todos modos
sigue siendo un mensaje incomprensible. Idea un metodo para extraer el mensaje
y guardalo en una variable llamada mensaje y mostrar el valor de la variable
en pantalla.

string = !XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"

"""

string = "!XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"

# Se eliminan las X del mensaje.
sin_x = string.split("X")
# El resultado es !egassem terces a si siht
sin_x.reverse()
# Luego de invertir el mensaje queda: this is a secret message!

# Se concatenan las letras para formar un string.
mensaje = ""
for letter in sin_x:
    mensaje += letter

# Se imprime el resultado.
print(mensaje)
