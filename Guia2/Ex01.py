"""
Ejercicio 1:

Dada un String que se forman de manera aleatoria, devuelva una nueva
lista cuyo contenido sea igual a la original pero de modo invertida.

"""

# Se importa la libreria random para seleccionar una frase al azar.
import random

# Lista que contiene varias oraciones para ser invertidas. Una sera elegida.
oraciones = [
            "Hola como te encuentras hoy",
            "Alexis le pegooo Chile campeon",
            "Esto le parece un pedazo de palo",
            "Mira mama estoy programado: 'Hola Mundo'",
            "Tragico accidente Leonardo Farkas lo pierde todo",
            "A esta oracion no le gusta que la inviertan :(",
            "Ejercicio 1 de la Guia 2 del ramo Programacion I"
            ]

# Con choice se escoge una oracion al azar.
palabras = random.choice(oraciones)
# Luego se separa en palabras para invertirla.
palabras = palabras.split()
palabras.reverse()

r_oracion = ""

# Cada palabra es concatenada en un nuevo string para ser imprimido.
for word in palabras:
    r_oracion += word + " "

# Se imprime la oracion invertida.
print(r_oracion)
