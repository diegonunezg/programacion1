# By Diego Núñez & Sebastián Bustamante

# -*- coding: utf-8 -*-
import random
import time

from colorama import init, Fore, Style

# Para iniciar colorama
init()

# Semilla de generacion de numeros aleatoreos.
random.seed()

# Esta variable define el tamaño de la matriz N*N
ORDEN = random.randint(18, 25)
# La siguiente expresion modela la cantidad de elementos
# maximos dependiendo del tamaño de la matriz.
n = ORDEN - 4
ELEMENTOS_MAXIMOS = int((1/8) * (2*(n**2) + 16*n + (-1)**n + 23))
# Esta variable define el tiempo que tarda la particula en moverse un paso.
# Se puede modificar para acelerar o desacelerar el tiempo entre iteraciones.
zzz = 0.25

# Se crean variables constantes que determinan elementos de la simulacion.
# Se les asigna un color a cada una para diferenciarlas del resto.
VACIO = " "
PARTICULA = Fore.MAGENTA + Style.BRIGHT + "*"  + Fore.RESET + Style.RESET_ALL
SUPER_PARTICULA = Fore.MAGENTA + Style.BRIGHT + "@" + Fore.RESET + Style.RESET_ALL
PARED = Fore.CYAN + "A" + Fore.RESET
TELEPORT = Fore.GREEN + "E" + Fore.RESET
COLAPSO = Fore.MAGENTA + Style.BRIGHT + "F" + Fore.RESET + Style.RESET_ALL
POWER_UP = Fore.RED + "S" + Fore.RESET
CAJA = Fore.YELLOW + "B" + Fore.RESET

ELEMENTOS_CREABLES = [PARED, TELEPORT, POWER_UP, CAJA]
ELEMENTOS_CHOCABLES = [PARED, CAJA]

# Esta funcion es la encargada de crear el espacio de la matriz y las letras.
# La funcion añade estos numeros en los indices con cero, por lo tanto no hay
# complicacion con restar uno a los indices, pues el primer item de
# la matriz estara en la posicion (1,1)
def crea_matriz():
	matriz = []
	coords = []
	objetos_creados = 0
	# La primera fila de la matriz contiene numeros desde 1 hasta ORDEN.
	for tmp in range(0, ORDEN+1):
		coords.append(str(tmp))
	matriz.append(coords)

	# Para cada fila siguiente, se agrega al principio,
	# el numero que corresponde a la fila en cuestion.
	for columna in range(1,ORDEN+1):
		if columna <= 9:
			filas = [" " + str(columna)]
		else:
			filas = [str(columna)]
		for fila in range(0, ORDEN):
			filas.append(VACIO)
		matriz.append(filas)

	# Una vez creada la matriz, antes de devolverla, se crean los elementos.
	# Solo se crea una letra F, y el resto se divide en las 4 letras posibles.
	crear_objetos(matriz, COLAPSO)
	objetos_creados += 1
	for objetos in range(1, int(ELEMENTOS_MAXIMOS//4)):
		crear_objetos(matriz, POWER_UP)
		crear_objetos(matriz, TELEPORT)
		crear_objetos(matriz, PARED)
		crear_objetos(matriz, CAJA)
		objetos_creados += 4
	return matriz, objetos_creados

# Esta funcion recibe la matriz y el objeto a crear deseado, y
# lo inserta en un espacio vacio de la matriz.
def crear_objetos(matriz = [" "], objeto_a_crear = VACIO):
	fila = random.randint(1, ORDEN)
	columna = random.randint(1, ORDEN)
	while matriz[fila][columna] != VACIO:
		fila = random.randint(1, ORDEN)
		columna = random.randint(1, ORDEN)
	matriz[fila][columna] = objeto_a_crear

# Esta funcion imprime la matriz de una forma mas atractiva visualmente.
def imprime_matriz(matriz = [" "]):
	print("\n")
	for tmp in range(0, len(matriz)):
		if tmp == 0:
			if ORDEN > 9:
				separar_primero = slice(10)
				separar_segundo = slice(10, len(matriz[tmp]))
				primero = matriz[tmp][separar_primero]
				segundo = matriz[tmp][separar_segundo]
				bonito = " " + "   ".join(primero) + "  " + "  ".join(segundo)
			else:
				bonito = " " + "   ".join(matriz[tmp])
		else:
			if tmp <= 9:
				bonito = " | ".join(matriz[tmp]) + " |"
			else:
				bonito = " | ".join(matriz[tmp]) + " |"
		print(bonito)
	print("\n")

# Esta funcion da origen a la particula en un lugar al azar del espacio.
def big_bang(matriz = [" "]):
	fila = random.randint(1, ORDEN)
	columna = random.randint(1, ORDEN)
	matriz[fila][columna] = PARTICULA
	return fila, columna

# Esta funcion tiene como objetivo otorgarle vida a la particula,
# dandole una direccion con la que empezar a moverse.
def vida_particula(matriz = [" "]):
	# La direccion inicial de la particula puede ser vertical(0)-horizontal(1)
	direccion = random.randint(0, 1)
	# Sentido indica el movimiento positivo o negativo para cada eje.
	sentido = [-1, 1]
	# Movimiento vertical: x = 1 or x = -1 ; y = 0
	if direccion == 0:
		x_axis = random.choice(sentido)
		y_axis = 0
	# Movimiento horizontal: x = 0 ; y = 1 or y = -1
	else:
		x_axis = 0
		y_axis = random.choice(sentido)
	return x_axis, y_axis

# Esta funcion es el corazon del programa, el movimiento de la particula
# y las interacciones con las letras, ocurren aqui.
def movimiento_particula(matriz = [" "], objetos_creados = 0):
	# Esta variable indica la forma actual de la particula. (normal o super)
	estado_particula = PARTICULA
	contador_superparticula = 0

	# Aparición de la particula e inicio del movimiento.
	fila, columna = big_bang(matriz)
	x_axis, y_axis = vida_particula(matriz)
	#imprime_matriz(matriz)
	time.sleep(zzz)

	# Variable que contara las iteraciones para que termine la simulacion.
	entropia = 0

	# Mientras la cantidad de iteraciones sea menor a 5000, no se detiene.
	while entropia < 5000:
		print(f"Entropia del sistema: {entropia}")

		# Si no se ha alcanzado la cantidad maxima de elementos, se crea otro.
		if objetos_creados < ELEMENTOS_MAXIMOS:
			crear_objetos(matriz, random.choice(ELEMENTOS_CREABLES))
			objetos_creados += 1

		# Si la particula se encuentra en su super
		# estado, se le resta 1 al contador.
		if contador_superparticula > 0:
			contador_superparticula -= 1
			# Si el contador llega a 0, la particula vuelve a la normalidad.
			if contador_superparticula <= 0:
				estado_particula = PARTICULA

		# La particula intenta moverse por lo que abandona su posicion.
		# Si choca, volvera a donde estaba.
		matriz[fila][columna] = VACIO
		"""
		Para evitar que la particula se atasque indefinidamente
		en los mismos rebotes, se plantean varias opciones de choque
		para minimizar este problema y asi lograr una particula
		con un movimiento mas complejo y caotico.

		"""
		# Choque de esquinas: superior izquierda.
		if (fila == 1 and columna == 1) and (x_axis == -1 and y_axis == -1):
			matriz[fila][columna] = estado_particula
			x_axis = random.randint(0, 1)
			if x_axis == 0:
				y_axis = 1
			else:
				y_axis = random.randint(0, 1)

		# Choque de esquinas: inferior derecha.
		elif (fila == ORDEN and columna == ORDEN) and (x_axis == 1 and y_axis == 1):
			matriz[fila][columna] = estado_particula
			x_axis = random.randint(-1, 0)
			if x_axis == 0:
				y_axis = -1
			else:
				y_axis = random.randint(-1, 0)

		# Choque de esquinas: superior derecha.
		elif (fila == 1 and columna == ORDEN) and (x_axis == -1 and y_axis == 1):
			matriz[fila][columna] = estado_particula
			x_axis = random.randint(0, 1)
			if x_axis == 0:
				y_axis = -1
			else:
				y_axis = random.randint(-1, 0)

		# Choque de esquinas: inferior izquierda.
		elif (fila == ORDEN and columna == 1) and (x_axis == 1 and y_axis == -1):
			matriz[fila][columna] = estado_particula
			x_axis = random.randint(-1, 0)
			if x_axis == 0:
				y_axis = 1
			else:
				y_axis = random.randint(0, 1)

		# Choque con la pared superior.
		elif fila == 1 and x_axis == -1:
			matriz[fila][columna] = estado_particula
			x_axis = 1
			y_axis = random.randint(-1, 1)

		# Choque con la pared inferior.
		elif fila == ORDEN and x_axis == 1:
			matriz[fila][columna] = estado_particula
			x_axis = -1
			y_axis = random.randint(-1, 1)

		# Choque con la pared izquierda.
		elif columna == 1 and y_axis == -1:
			matriz[fila][columna] = estado_particula
			x_axis = random.randint(-1, 1)
			y_axis = 1

		# Choque con la pared derecha.
		elif columna == ORDEN and y_axis == 1:
			matriz[fila][columna] = estado_particula
			x_axis = random.randint(-1, 1)
			y_axis = -1

		# No hay choque.
		else:
			"""
			Si la particula no choca con un borde,
			delante tendra una letra o un espacio vacio.

			"""
			# La particula logro moverse.
			fila += x_axis
			columna += y_axis

			# Si al avanzar, hay una letra F, el programa termina.
			if matriz[fila][columna] == COLAPSO:
				matriz[fila][columna] = Fore.MAGENTA + "#" + Fore.RESET
				break

			# Si al avanzar, hay una letra A o B, y la particula no
			# esta en su super estado, entonces rebota.
			elif matriz[fila][columna] in ELEMENTOS_CHOCABLES:
				# Si la particula no esta en su super estado, colisiona.
				if estado_particula == PARTICULA:
					# Si el objeto colisionado es una letra B,
					# desplaza la letra solo si es posible.
					if matriz[fila][columna] == CAJA:
						try:
							if matriz[fila+x_axis][columna+y_axis] == VACIO:
								matriz[fila][columna] = VACIO
								matriz[fila+x_axis][columna+y_axis] = CAJA
						except IndexError:
							pass

					# Cuando la particula choca con un objeto que altere su
					# direccion, modifica su trayectoria de 3 formas posibles:
					# 0: rebota sobre su mismo eje, solo cambiando su sentido.
					# 1: rebota girando su direccion en sentido antihorario.
					# -1: rebota girando su direccion en sentido horario.
					fila -= x_axis
					columna -= y_axis
					tipo_choque = random.randint(-1, 1)
					if tipo_choque == 0:
						x_axis *= -1
						y_axis *= -1
					else:
						# Si el movimiento es no diagonal, se le suma el
						# tipo de choque al eje asociado.
						if x_axis == 0 or y_axis == 0:
							if x_axis == 0:
								x_axis += tipo_choque
							else:
								y_axis += tipo_choque
						# Si el tipo de movimiento es diagonal, una coordenada
						# asociada al movimiento se convierte en 0
						else:
							if tipo_choque == -1:
								x_axis = 0
							else:
								y_axis = 0
					matriz[fila][columna] = estado_particula
				
				# Si esta en super estado, la particula se come lo que encuentre.
				else:
					objetos_creados -= 1

			# Si al avanzar, hay una letra E, la particula es teletransportada.
			elif matriz[fila][columna] == TELEPORT:
				matriz[fila][columna] = VACIO
				aux = [1, ORDEN]
				fila = random.choice(aux)
				columna = fila
				matriz[fila][columna] = estado_particula
				objetos_creados -= 1

			# Si al avanzar, hay una letra S, la particula pasa a su super estado.
			if matriz[fila][columna] == POWER_UP:
				if estado_particula != SUPER_PARTICULA:
					estado_particula = SUPER_PARTICULA
					contador_superparticula = 10
					objetos_creados -= 1
				else:
					objetos_creados -= 1

			# Al final de todos los posibles encuentros
			# con letras, la particula se mueve.
			matriz[fila][columna] = estado_particula
		imprime_matriz(matriz)
		time.sleep(zzz)
		entropia += 1

		# Si las iteraciones alcanzan su limite, un simpatico mensaje
		# es impreso por pantalla y la simulacion termina.
		if entropia >= 5000:
			print("\n\n" * 3)
			print("El sistema se volvio demasiado caotico...")
			time.sleep(2)
			print("El nivel de entropia es demasiado alto...")
			time.sleep(2)
			print("Se acerca el colapso del universo...")
			time.sleep(2)
			matriz = muerte_matriz()

	return matriz

# Esta funcion solo es utilizada si el programa termina por limite de iteraciones.
def muerte_matriz():
	matriz = []
	coords = []
	caos = ["%", "#", "@", "&", "$", "!"]
	# La primera fila de la matriz contiene numeros desde 1 hasta ORDEN.
	for tmp in range(0, ORDEN+1):
		coords.append(str(tmp))
	matriz.append(coords)

	# Para cada espacio en la matriz, se crea un simbolo al azar de la lista caos.
	# Su funcion es meramente estetica y para hacer mas simpatico el programa.
	for fila in range(1,ORDEN+1):
		if fila <= 9:
			filas = [" " + str(fila)]
		else:
			filas = [str(fila)]
		for columna in range(0, ORDEN):
			filas.append(random.choice(caos))
		matriz.append(filas)
	return matriz

# Se define la funcion principal, que ejecutara todo el programa en su conjunto.
def main():
	universo, objetos_creados = crea_matriz()
	time.sleep(1)
	print("\n\n")
	universo = movimiento_particula(universo, objetos_creados)
	imprime_matriz(universo)

if __name__ == "__main__":
	main()
