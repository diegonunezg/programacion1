# By: Diego Núñez

"""

			PROBLEMA:

La loteria: un estudiante que quiere apostar, escoge C numeros distintos de
1 a K y paga 1000 pesos. El lunes antes de ingresar a clases, C numeros
(de 1 a K) son extraıdos. El estudiante cuya apuesta tiene el numero mas grande
de aciertos recibe la cantidad de las apuestas. Esta cantidad es compartida en
caso de empate y se acumula a la proxima semana si nadie adivina cualquiera de
los numeros extraıdos.

Algunos de sus compañeros no creen en las leyes de probabilidad, y desean que
usted ESCRIBA UN PROGRAMA QUE DERERMINE LOS NUMEROS A ESCOGER, considerando los
numeros que MENOS SALIERON EN SORTEOS ANTERIORES, de modo que ellos puedan
apostar a aquellos numeros.

Para diseñar el programa existen dos consideraciones: la entrada y salida.

ENTRADA:
Una entrada contiene varios casos de prueba. El primer ingreso de un caso de
prueba contiene tres numeros enteros N, C y K que indican, respectivamente:
el numero de sorteos que ya han pasado (1 <= N <= 1000), cuantos numeros
comprenden una apuesta (1 <= C <= 10) y el valor maximo de los numeros que
pueden ser escogidos en una apuesta (C <= K <= 100). Luego le siguen N nuevas
entradas que contienen C numeros enteros distintos, Xi que indica los numeros
extraıdos en cada sorteo anterior (1 <= Xi <= K; para 1 <= i <= C).

SALIDA:
Para cada caso de prueba en la entrada su programa debe escribir una linea
de salida, conteniendo el juego de numeros que han sido han salido la menor
cantidad de veces. Este juego debe ser impreso como una lista, en orden
creciente de numeros. Inserte un espacio en blanco entre dos numeros
consecutivos en la lista.

			ANALISIS:

Se tiene una cantidad N de sorteos, donde se eligen al azar C numeros para
apostar, cuyos valores se encuentran entre 1-K.

- ENTRADA:

Para la generacion de numeros aleatoreos se usara la libreria random.
Por cada sorteo, se generara una lista que contenga los numeros ganadores,
y estas listas, seran guardadas a su vez dentro de otra lista que las
contendra a todas.

Para la evaluacion de cuales numeros fueron los que tuvieron menos
ocurrencias, se creara una lista con K elementos, los cuales inicialmente
tendran un valor 0, y luego, cada vez que un numero x ocurra, se le sumara 1
al valor que se encuentre en el indice (x-1).

Por ejemplo, para K = 9, se crea esta lista:

[0, 0, 0, 0, 0, 0, 0, 0, 0]

Y luego, cuando un numero salga sorteado (por ejemplo el 3) se sumara 1,
quedando la lista de esta forma:

[0, 0, 1, 0, 0, 0, 0, 0, 0]

Asi, al finalizar todos los sorteos, se tendran contabilizadas las ocurrencias
de cada numero en orden ascendiente desde 1 a K. Esto es importante, y sera
utilizado a la hora de entregar la salida.

- SALIDA:

Dentro de los aspectos a considerar del problema, se encuentran los siguientes:
 - La forma de expresar la solucion es propia y de libre interpretacion.
 - No puede usar el metodo sort() para las listas.


Teniendo esto en cuenta, mi interpretacion para la salida es la siguiente:

Dado que se pide entregar los numeros que menos veces ocurrieron en orden
ascendiente, solo aquellos numeros que compartan un menor numero de ocurrencias,
seran los entregados al finalizar, ya sea un solo numero o varios.
A lo que hace referencia esto, es que, por ejemplo para K = 9, si la lista
final de OCURRENCIAS es de esta forma

[1, 0, 5, 3, 4, 0, 1, 7, 4] <-- Lista de ocurrencias de cada numero
 1  2  3  4  5  6  7  8  9  <-- Numero relacionado

Solo sera impreso el numero 2 y el 6 como los de menor ocurrencia (0 veces),
ya que, si tambien se incluyen los numeros 1 y 7 que tienen (1) ocurrencia,
al ordenarlos ascendentemente quedarian: (1, 2, 6, 7) pero el 2 y el 6
aparecieron menos veces que los demas, y esta informacion se perderia.


Ahora, recordando que la utilizacion de el metodo sort() esta prohibida,
para entregar los numeros en orden creciente, el programa buscara aquellos
casos con menor ocurrencia, partiendo con aquellos que no salieron sorteados
en ningun momento. En el caso de que no encuentre un numero con 0 ocurrencias,
el programa buscara aquellos que hayan salido 1 vez, si no hay ninguno,
pasara a buscar todos los que salieron 2 veces, y asi hasta que encuentre
la menor ocurrencia de todas.
Una vez encuentre este valor, el programa recorrera en orden la lista de
ocurrencias, y cuando haya una coincidencia, el programa guardara el numero
asociado a dicho valor, lo que vendria siendo el indice + 1.

Tomando el ejemplo anterior de una lista con K = 9: [1, 0, 5, 3, 4, 0, 1, 7, 4]
El programa solo se quedaria con los indices cuyo contenido contengan un 0,
en este caso lista[1] y lista[5] contienen un 0, por lo que los numeros
asociados serian el resultado de sumar 1 a sus indices, osea: 2 y 6
Estos numeros son guardados en una lista nueva, y dado que la lista de
ocurrencias estaba ordenada previamente y que el programa recorre la lista en
orden de izquierda a derecha, no seria necesario utilizar el metodo sort(),
puesto que los resultados ya estan ordenados, entregando la solucion:

> Los siguientes numeros no aparecieron durante los sorteos:
  2 6

"""

# Se importa la libreria random para la generacion de numeros aleatoreos.
import random

# Se establece la semilla de generacion de numeros aleatoreos.
random.seed()

# Funcion que retorna una lista con los numeros que salieron x veces en los sorteos.
def target_number(x = 0):
	lista_numeros = []
	for i in range(0, K):
		if ocaciones[i] == x:
			# Los numeros a agregar a la lista son convertidos a string para
			# luego facilitar el uso del metodo join() al imprimir por pantalla.
			agregar = str(i + 1)
			lista_numeros.append(agregar)
	return lista_numeros

# Esta funcion transforma los elementos en una lista de la forma
# ["a", "b", ..., "n"] a una cadena de texto de la forma "a b ... n".
def list_to_string(list = []):
	# El metodo join() une todos los elementos de una lista y los convierte en
	# un string, donde " " es el separador entre cada elemento.
	stringlist = " ".join(list)
	return stringlist

# Esta funcion se encarga de realizar los sorteos.
def sortear():
	# Lista que contendra todas las instancias de sorteo.
	sorteos= []
	# Este ciclo genera una lista con los numeros de cada sorteo,
	# y guarda cada una de estas, dentro de la lista sorteos.
	for i in range(0, N):
		instancia = []
		for pos in range(0, C):
			# Este ciclo siempre se ejecuta y se encarga de que los numeros no se repitan.
			while True:
				azar = random.randint(1, K)
				# Si la lista del sorteo actual no tiene elementos escapa del ciclo.
				if len(instancia) == 0:
					break
				# Si el numero aleatoreo ya fue elegido previamente, vuelve a elegir otro distinto.
				elif not instancia.count(str(azar)) == 0:
					continue
				else:
					break
			# Cuando un numero es elegido exitosamente, se añade 1 a sus ocurrencias.
			ocaciones[azar - 1] += 1
			instancia.append(str(azar))
		sorteos.append(instancia)
	return sorteos

# Se le pide al usuario ingresar los datos necesarios para los sorteos, y se manejan excepciones.
while True:
	try:
		N = int(input("Ingrese el numero de sorteos que ya han sido jugados con anterioridad (1 - 1000): "))
		if N < 1 or N > 1000:
			print(f"\nEl numero ingresado {N} no se encuentra en el rango especificado (1 - 1000)")
			print("Intentelo nuevamente.\n")
		else:
			break
	except ValueError:
		print("\nEl valor ingresado no es un numero entero positivo, intentelo nuevamente.\n")
		continue

while True:
	try:
		C = int(input("Ingrese la cantidad de numeros que cada jugador elegia por sorteo (1 - 10): "))
		if C < 1 or C > 10:
			print(f"\nEl numero ingresado {C} no se encuentra en el rango especificado (1 - 10)")
			print("Intentelo nuevamente.\n")
		else:
			break
	except ValueError:
		print("\nEl valor ingresado no es un numero entero positivo, intentelo nuevamente.\n")
		continue

while True:
	try:
		K = int(input(f"Ingrese el numero con valor mas alto de todos los numeros a sortear ({C} - 100): "))
		if K < C or K > 100:
			print(f"\nEl numero ingresado {K} no se encuentra en el rango especificado ({C} - 10)")
			print("Intentelo nuevamente.\n")
		else:
			break
	except ValueError:
		print("\nEl valor ingresado no es un numero entero positivo, intentelo nuevamente.\n")
		continue

# Se crea una lista que contendra la cantidad de veces que un numero
# aparecio, donde el numero corresponde al indice + 1.
ocaciones = []
for i in range(0, K):
	ocaciones.append(0)

# Se realizan los sorteos de prueba.
sorteos = sortear()

# Este ciclo imprime una linea por cada uno de los sorteos realizados (N)
print("\nLos resultados de los sorteos fueron:")
for i in range(0, N):
	print(list_to_string(sorteos[i]))

# Si hay numeros entre 1 y K que no aparecieron en ningun sorteo, se imprime una lista con estos numeros.
if ocaciones.count(0) != 0:
	print("\nLos siguientes numeros no aparecieron durante los sorteos: ")
	print(list_to_string(target_number(0)))
# Sino, se imprime esta info y luego la lista de numeros con la menor ocurrencia.
else:
	print("\nDato curioso: todos los numeros salieron como minimo 1 vez.")
	for i in range(1, N+1):
		if ocaciones.count(i) != 0:
			print(f"Los numeros que menos aparecieron, lo hicieron {i} veces, y estos fueron: ")
			print(list_to_string(target_number(i)))
			break

