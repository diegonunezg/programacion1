# by Diego Núñez González

"""
Una farmaceutica lleva informacion de todos sus medicamentos en un archivo.
Esta informacion se encuentra ordenada en funcion del laboratorio que crea el
farmaco, por lo que existe un archivo por cada laboratorio que podamos crear.
La forma de como guarda la informacion el laboratorio en cada uno de los
archivos es la siguiente:
    - Codigo del producto
    - Nombre del Farmaco
    - Componente principal
    - Precio de venta
Escriba un programa en que realice las siguientes operaciones:

a) Permitir agregar medicamentos con toda su informacion a los diferentes
   archivos dependiendo del laboratorio.
b) Permitir consultar precio de un medicamento.
c) Permitir editar un medicamento de un archivo en particular.
d) Eliminar un registro de medicamento en particular.
e) Mostrar la informacion de los laboratorios registrados (para esto cree un
   archivo solo con el nombre del laboratorio y este sea el nombre del archivo
   a trabajar).
f) Mostrar alternativa de precio mas baja para un medicamentos con un mismo
   componente principal, esto previa consulta.

--------------------------------------------------------------------------------

La solucion propuesta es independiente de cualquier archivo, es decir, no es
necesario tener los archivos pre-creados, ya que si no existen, el programa
los crea sobre la marcha y con el mismo formato para todos.

Para evitar problemas con las strings, todas las instancias de entrada de
texto que se guardan, estan formateadas con el metodo .capitalize()

"""

from pathlib import Path
import random
import time

# Esta variable globaliza el tiempo de espera del comando sleep.
zzz = 0.75

# Se crea la lista laboratorios, que contendra, como su nombre
# indica, los laboratorios existentes. En caso de que no exista
# un archivo con estos, crea uno con estos labs por defecto.
laboratorios = ["Pfizer", "Bayer", "Johnson"]

# A la hora de crear un laboratorio nuevo, cierta cantidad
# de medicamentos preestablecidos son elegidos al azar, para
# facilitar la ejecucion al no tener que crearlos manualmente.
medicamentos_gen = [{"Ibuprofeno": {"Componente": "Ibuprofeno"}},
                    {"Actron": {"Componente": "Ibuprofeno"}},
                    {"Paracetamol": {"Componente": "Paracetamol"}},
                    {"Tapsin": {"Componente": "Paracetamol"}},
                    {"Viadil": {"Componente": "Pargeverina clorhidrato"}},
                    {"Viproxil": {"Componente": "Pargeverina clorhidrato"}},
                    {"Jarabe tos": {"Componente": "Bromhexina hidrocloruro"}},
                    {"Bisolvon": {"Componente": "Bromhexina hidrocloruro"}},
                    {"Antiacido": {"Componente": "Carbonato de calcio"}},
                    {"Nogastrol": {"Componente": "Carbonato de calcio"}},
                    {"Prolopa": {"Componente": "L-dopa"}},
                    {"Levofamil": {"Componente": "L-dopa"}}
                    ]

# Se almacenaran los codigos de medicamentos usados para que no se repitan.
codigos = []


# Esta funcion se ejecuta al inicio del programa y recopila los laboratorios.
# Si no existe archivo con los laboratorios, crea uno.
def inicia_labs():
    try:
        with open("labs.txt", "r") as archivo:
            for linea in archivo:
                if linea != "\n" and linea[0:-1] not in laboratorios:
                    laboratorios.append(linea[0:-1])

    except FileNotFoundError:
        with open("labs.txt", "w") as archivo:
            texto = ""
            for item in laboratorios:
                texto += item + "\n"
            archivo.write(texto)


# Esta funcion modifica el archivo que contiene los nombres de laboratorios.
def modifica_labs():
    with open("labs.txt", "w") as archivo:
        for item in range(0, len(laboratorios)):
            archivo.writelines(laboratorios[item] + "\n")


# Esta funcion crea el formato para los archivos de cada laboratorio.
def ficheros_lab(archivo=None):
    # Se comprueba la existencia del archivo. Si no existe un archivo para
    # ese laboratorio, crea uno con una seleccion de medicamentos genericos.
    ex = Path(archivo)
    if not ex.exists():
        with open(archivo, "w") as fichero:
            catalogo = []
            # Se eligen 6 medicamentos genericos distintos al azar.
            while len(catalogo) < 6:
                eleccion = random.choice(medicamentos_gen)
                if not eleccion in catalogo:
                    catalogo.append(eleccion)
            # A cada medicamento se le asigna un codigo y un precio.
            for item in catalogo:
                for key in item:
                    # Se le asigna un codigo que no este en uso.
                    while True:
                        codigo = random.randint(1000, 9999)
                        if not codigo in codigos:
                            break
                    item[key]["Codigo"] = f"{codigo}"
                    codigos.append(str(codigo))
                    # Se le asigna un precio.
                    precio = random.randint(300, 1500) * 10
                    item[key]["Precio"] = f"{precio}"
                    comp = item[key]["Componente"]
                    # Se crea la linea a escribir en el archivo.
                    texto = f"{key}:{comp}:{codigo}:{precio}\n"
                    fichero.writelines(texto)
    # Si el archivo existe, la funcion se encarga
    # de recopilar los codigos que ya estan en uso.
    else:
        with open(archivo, "r") as fichero:
            catalogo = list(fichero)
        for linea in catalogo:
            tmp = linea.split(":")
            # Pop elimina un elemento de una lista y lo retorna.
            cod = tmp.pop(2)
            # Se añaden los codigos ya usados.
            codigos.append(cod)


# Esta funcion se encarga de seleccionar el nombre del archivo a abrir.
def elegir_archivo():
    # A cada laboratorio existente, se le asigna un numero,
    # de estos, el usuario debera elegir uno para abrir.
    for num, lab in enumerate(laboratorios):
        print(f"< {num+1} > {lab}")

    while True:
        try:
            opcion = int(input("\nSelecciona el laboratorio deseado: "))
        except ValueError:
            print("Comando invalido.")
        if opcion > 0 and opcion <= len(laboratorios):
            break
        else:
            print("Numero fuera de rango.")

    return opcion


# Esta funcion permite añadir un nuevo laboratorio a la lista.
def add_lab():
    print("\nOPCION 1: Agregar un laboratorio:\n")
    lab = input("Ingrese el nombre del laboratorio: ").capitalize()

    # Si el laboratorio a crear, no esta en la
    # lista de los ya existentes, lo agrega.
    if not lab in laboratorios:
        laboratorios.append(lab)
        modifica_labs()
        ficheros_lab(lab + ".txt")
    else:
        print("Este lab ya existe en el registro.")


# Esta funcion agrega un medicamento al archivo del laboratorio elegido.
def agrega_medicamento():
    print("\nOPCION 2: Agregar medicamento:\n")
    time.sleep(zzz)
    opcion = elegir_archivo()

    with open(laboratorios[opcion-1] + ".txt", "a") as fichero:
        # Se elige un nombre para el nuevo medicamento.
        while True:
            nombre = input("\nIngrese el nombre del medicamento a agregar: ")
            if nombre == "" or nombre.startswith(" "):
                print("El medicamento debe tener un nombre valido.")
            else:
                nombre = nombre.capitalize()
                break
        # Se elige el componente activo del nuevo medicamento.
        while True:
            comp = input("\nIngrese el componente activo del medicamento: ")
            if comp == "" or comp.startswith(" "):
                print("Este campo debe tener un nombre valido.")
            else:
                comp = comp.capitalize()
                break
        # Se elige el codigo del nuevo medicamento.
        while True:
            try:
                codigo = int(input("\nIngrese el codigo del medicamento: "))
                if len(str(codigo)) != 4:
                    print("El codigo debe ser un numero de 4 digitos.")
                    continue
            except ValueError:
                print("El codigo debe ser un dato numerico de 4 digitos.")
                continue
            # El codigo tiene que ser uno no usado.
            if codigo in codigos:
                print("Ya existe un medicamento con este codigo.")
            else:
                codigos.append(str(codigo))
                break
        # Se elige el precio del nuevo medicamento.
        while True:
            try:
                precio = int(input("\nIngrese el precio del medicamento: "))
            except ValueError:
                print("Ingrese un valor numerico.")
                continue
            # No se le puede otorgar un precio menor a cero.
            if precio <= 0:
                print("El precio debe ser un valor positivo.")
            else:
                break
        # Se escribe una linea con todos los datos al final del archivo.
        fichero.writelines(f"{nombre}:{comp}:{codigo}:{precio}\n")


# Esta funcion se encarga de consultar el precio de un medicamento especifico.
def consulta_precio():
    print("\nOPCION 3: Consultar precio de un medicamento:\n")
    time.sleep(zzz)
    opcion = elegir_archivo()
    print("\n")

    # Se abre el archivo del laboratorio seleccionado.
    with open(laboratorios[opcion-1] + ".txt", "r") as fichero:
        med_lab = list(fichero)

    # Se crea una lista auxiliar que contiene los nombres
    # de todos los medicamentos del laboratorio escogido.
    tmp = []
    for item in med_lab:
        # El nombre se encuentra al principio y se
        # marca su fin con una separacion con ':'
        aux = item.index(":")

        # Esta linea imprime el numero asociado al nombre del medicamento.
        print(f"< {med_lab.index(item) + 1} > {item[0:aux]}")
        tmp.append(f"{item[0:aux]}")

    # Una vez los medicamentos son filtrados, el usuario
    # escoge el medicamento que necesita saber el precio.
    while True:
        try:
            cons = int(input("\nIngrese el medicamento que desea consultar: "))
        except ValueError:
            print("Comando invalido.")
            continue
        if cons <= 0 or cons > len(tmp):
            print("Numero fuera de rango.")
        else:
            break

    # El precio se encuentra en la ultima columna de datos. Con [0:-1] se
    # elimina el salto de linea al final del texto. Con .split(":") se separa
    # la string en una lista con las 4 propiedades del medicamento, y
    # con .pop(3) se obtiene el ultimo elemento de la lista que es el precio.
    precio = med_lab[cons-1][0:-1].split(":").pop(3)
    print(f"\nEl medicamento {tmp[cons-1]} tiene un valor de {precio} pesos.\n")
    time.sleep(zzz)


# Esta funcion permite editar la informacion de un medicamento.
def editar_medicamento():
    print("\nOPCION 4: Editar un medicamento:\n")
    time.sleep(zzz)
    opcion = elegir_archivo()
    print("\n")

    # Se abre el archivo deseado y se conserva el texto.
    with open(laboratorios[opcion-1] + ".txt", "r") as fichero:
        med_lab = list(fichero)

    # Se crea una lista auxiliar para mostrar en pantalla los medicamentos
    # disponibles para ese laboratorio y el numero asociado a este.
    tmp = []
    for item in med_lab:
        aux = item.index(":")
        print(f"< {med_lab.index(item) + 1} > {item[0:aux]}")
        tmp.append(f"{item[0:aux]}")

    # El usuario elige el medicamento que quiere modificar.
    while True:
        try:
            edit = int(input("\nIngrese el medicamento que desea editar: "))
        except ValueError:
            print("Comando invalido.")
            continue
        if edit <= 0 or edit > len(tmp):
            print("Numero fuera de rango.")
        else:
            break

    # Se separan los datos en una lista.
    edicion = med_lab[edit-1][0:-1].split(":")

    # Si se desea dejar un parametro sin modificar, en vez de escribirlo
    # nuevamente, el usuario puede dejar el campo en blanco.
    print("\nSi no desea modificar un parametro, deje esa casilla en blanco.")

    # Se ingresa un nuevo nombre.
    n = input(f"\nIngrese el nuevo nombre (Antes = {edicion[0]}): ")
    if n == "":
        n = edicion[0]
    else:
        n.capitalize()

    # Se ingresa un nuevo componente.
    a = input(f"\nIngrese el nuevo componente (Antes = {edicion[1]}): ")
    if a == "":
        a = edicion[1]
    else:
        a.capitalize()

    # Se ingresa un nuevo codigo.
    while True:
        try:
            c = input(f"\nIngrese el codigo (Antes = {edicion[2]}): ")
            if c == "":
                c = edicion[2]
                break
            else:
                c = int(c)
                # El codigo debe tener 4 digitos.
                if len(str(c)) != 4:
                    print("Entrada no valida. Ingrese un codigo de 4 digitos.")
                    continue

                # El codigo no puede estar en uso.
                if c in codigos and c != edicion[2]:
                    print("Ese codigo ya esta en uso.")
                    continue
                x = codigos.index(edicion[2])
                # Se elimina el codigo antiguo y se agrega el nuevo.
                codigos.pop(x)
                codigos.append(c)
                break

        except ValueError:
            print("Codigo no valido. Ingrese un dato numerico de 4 digitos.")

    # Se ingresa un nuevo precio.
    while True:
        try:
            p = input(f"\nIngrese el precio (Antes = {edicion[3]}): ")
            if p == "":
                p = edicion[3]
                break
            p = int(p)
            if p <= 0:
                print("Precio no valido. Ingrese un numero mayor a cero.")
                continue
            else:
                break

        except ValueError:
            print("Codigo no valido. Ingrese un dato numerico de 4 digitos.")

    # Se modifican los valores del medicamento y se reescriben.
    med_lab[edit-1] = f"{n}:{a}:{c}:{p}\n"
    with open(laboratorios[opcion-1] + ".txt", "w") as fichero:
        for item in med_lab:
            fichero.writelines(item)
    print("\nMedicamento editado satisfactoriamente.\n")


# Esta funcion permite eliminar un medicamento de un laboratorio.
def eliminar_medicamento():
    print("\nOPCION 5: Eliminar medicamento:\n")
    time.sleep(zzz)
    opcion = elegir_archivo()
    print("\n")

    # Se abre el archivo del laboratorio escogido, y se recoge su contenido.
    with open(laboratorios[opcion-1] + ".txt", "r") as fichero:
        med_lab = list(fichero)

    # Se crea una lista temporal, para recoger el nombre de los
    # medicamentos y otorgarles un numero para que el usuario escoja.
    tmp = []
    for item in med_lab:
        aux = item.index(":")
        print(f"< {med_lab.index(item) + 1} > {item[0:aux]}")
        tmp.append(f"{item[0:aux]}")

    # Este ciclo gestiona la eliminacion del medicamento.
    while True:
        try:
            delete = int(input("\nIngrese el medicamento que desea eliminar: "))
        except ValueError:
            print("Comando invalido.")
            continue
        if delete <= 0 or delete > len(tmp):
            print("Numero fuera de rango.")
        else:
            med_lab.pop(delete-1)
            # Se reescriben los datos.
            with open(laboratorios[opcion-1] + ".txt", "w") as fichero:
                for item in med_lab:
                    fichero.writelines(item)
            print("\nMedicamento eliminado satisfactoriamente.")
            break


# Esta funcion se encarga de entregar toda la
# informacion relacionada al laboratorio escogido.
def info_labs():
    print("\nOPCION 6: Mostrar informacion de laboratorio:\n")
    time.sleep(zzz)
    opcion = elegir_archivo()
    print("\n")

    # Se abre el archivo y se recopilan sus datos.
    with open(laboratorios[opcion-1] + ".txt", "r") as fichero:
        texto = list(fichero)

    # Por cada medicamento, separa sus propiedades
    # y las imprime por pantalla en una linea.
    for linea in range(0, len(texto)):
        detalles = texto[linea][0:-1].split(":")
        n = detalles[0]
        a = detalles[1]
        c = detalles[2]
        p = detalles[3]
        print(f"Nombre: {n}, Componente: {a}, Codigo: {c}, Precio: {p}")

    # No se vuelve inmediatamente al menu, para
    # que el usuario pueda leer los datos.
    try:
        input("\nPresione cualquier tecla para volver al menu: ")
    except:
        pass


# Esta funcion, busca todos los medicamentos con el mismo principio
# activo, los imprime por pantalla, e indica el mas barato de todos ellos.
def compara_precios():
    print("\nOPCION 7: Busqueda de precio mas bajo para un medicamento:\n")

    componentes = []
    info = []
    medicamentos = []

    # Por cada laboratorio, se almacenan todos los medicamentos
    # con su respectivo laboratorio y otros datos.
    for lab in laboratorios:
        # Se abre el archivo del laboratorio a examinar.
        with open(lab + ".txt", "r") as fichero:
            texto = list(fichero)

        # Se obtienen los datos de cada medicamento.
        for linea in texto:
            precio = linea[0:-1].split(":").pop(3)
            codigo = linea[0:-1].split(":").pop(2)
            comp = linea[0:-1].split(":").pop(1)
            nombre = linea[0:-1].split(":").pop(0)

            # Si el medicamento examinado, es el primero en
            # mostrar un componente activo distinto, se agrega a
            # la lista que contendra todos estos, una vez cada uno.
            if not comp in componentes:
                componentes.append(comp)

            # Finalmente, se agrega el medicamento
            # a la lista de todos los medicamentos.
            info.append([comp, nombre, lab, codigo, precio])
    print("\n")

    # A cada componente activo distinto, se le asocia un numero para
    # que el usuario consulte todos los medicamentos que lo contienen
    for num, med in enumerate(componentes):
        print(f"< {num+1} > {med}")

    # Este ciclo gestiona la eleccion del usuario.
    while True:
        try:
            opcion = int(input("\nIngrese el componente deseado: "))
            if opcion <= 0 or opcion > len(componentes):
                print("Entrada fuera de rango.")
                continue
            else:
                break
        except ValueError:
            print("Entrada invalida.")

    # Cada medicamento que contenga el principio activo
    # especificado, es agregado a una lista para luego ser impreso.
    for tm in info:
        if tm[0] == componentes[opcion-1]:
            medicamentos.append(tm)

    # Se entregan los resultados.
    print(f"\nPara la busqueda con el componente {componentes[opcion-1]}")
    print("se obtuvieron los siguientes resultados:\n")
    min = int(medicamentos[0][4])
    pb = medicamentos[0]
    for tm in medicamentos:
        if int(tm[4]) < min:
            min = int(tm[4])
            pb = tm
        print(f"{tm[1]}, precio {tm[4]}, laboratorio {tm[2]}, codigo {tm[3]}")

    print("\nLa alternativa de precio mas baja es: ")
    print(f"{pb[1]}, precio {pb[4]}, laboratorio {pb[2]}, codigo {pb[3]}")

    # No se vuelve inmediatamente al menu, para
    # que el usuario pueda leer los datos.
    try:
        input("\nPresione cualquier tecla para volver al menu: ")
    except:
        pass


# Esta funcion se encarga de gestionar el programa y darle opciones al usuario.
def gestion_programa():
    while True:
        time.sleep(zzz)
        print("\nBienvenido al menu de la base de datos.\n")
        print("< 1 > Agregar un laboratorio")
        print("< 2 > Agregar un medicamento")
        print("< 3 > Consultar precio de un medicamento")
        print("< 4 > Editar un medicamento")
        print("< 5 > Eliminar un medicamento")
        print("< 6 > Mostrar informacion de laboratorio")
        print("< 7 > Busqueda de precio mas bajo para un medicamento")
        print("< 8 > Finalizar ejecucion")

        # Se elige una opcion y se manejan excepciones.
        try:
            opcion = int(input("\nQue accion desea realizar: "))
            time.sleep(zzz)
            if opcion == 1:
                add_lab()
            elif opcion == 2:
                agrega_medicamento()
            elif opcion == 3:
                consulta_precio()
            elif opcion == 4:
                editar_medicamento()
            elif opcion == 5:
                eliminar_medicamento()
            elif opcion == 6:
                info_labs()
            elif opcion == 7:
                compara_precios()
            elif opcion == 8:
                break
            else:
                print("La opcion elegida no es valida.")

        except ValueError:
            print("\nLa opcion elegida no es valida.")


# Se define la funcion principal del programa.
def main():
    # Se llama la funcion que cargara los laboratorios existentes.
    inicia_labs()
    # Por cada laboratorio, se abre (o se crea) un archivo.
    for lab in laboratorios:
        ficheros_lab(lab + ".txt")
    # Luego de que el programa se asegura de que los archivos existen,
    # se puede empezar a trabajar con las bases de datos.
    gestion_programa()


if __name__ == "__main__":
    main()
