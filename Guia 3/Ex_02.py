# by Diego Núñez González

"""
Una farmaceutica lleva informacion de todos sus medicamentos en un archivo.
Esta informacion se encuentra ordenada en funcion del laboratorio que crea el
farmaco, por lo que existe un archivo por cada laboratorio que podamos crear.
La forma de como guarda la informacion el laboratorio en cada uno de los
archivos es la siguiente:
    - Codigo del producto
    - Nombre del Farmaco
    - Componente principal
    - Precio de venta
Escriba un programa en que realice las siguientes operaciones:

a) Permitir agregar medicamentos con toda su informacion a los diferentes
   archivos dependiendo del laboratorio.
b) Permitir consultar precio de un medicamento.
c) Permitir editar un medicamento de un archivo en particular.
d) Eliminar un registro de medicamento en particular.
e) Mostrar la informacion de los laboratorios registrados (para esto cree un
   archivo solo con el nombre del laboratorio y este sea el nombre del archivo
   a trabajar).
f) Mostrar alternativa de precio mas baja para un medicamentos con un mismo
   componente principal, esto previa consulta.

--------------------------------------------------------------------------------

La solucion propuesta es independiente de cualquier archivo, es decir, no es
necesario tener los archivos pre-creados, ya que si no existen, el programa
los crea sobre la marcha y con el mismo formato para todos.

Para evitar problemas con las strings, todas las instancias de entrada de
texto que se guardan, estan formateadas con el metodo .capitalize()

"""

from pathlib import Path
import json
import random
import time

# Esta variable globaliza el tiempo de espera del comando sleep.
zzz = 0.75

# Se crea la lista laboratorios, que contendra, como su nombre
# indica, los laboratorios existentes. En caso de que no exista
# un archivo labs.json, crea uno con estos labs por defecto.
labs = ["Pfizer", "Bayer", "Johnson"]

# A la hora de crear un laboratorio nuevo, cierta cantidad
# de medicamentos preestablecidos, son elegidos al azar, para
# facilitar la ejecucion al no tener que crearlos manualmente.
medicamentos_gen = [{"Nombre": "Ibuprofeno", "Componente": "Ibuprofeno"},
                    {"Nombre": "Actron", "Componente": "Ibuprofeno"},
                    {"Nombre": "Paracetamol", "Componente": "Paracetamol"},
                    {"Nombre": "Tapsin", "Componente": "Paracetamol"},
                    {"Nombre": "Viadil", "Componente": "Pargeverina clorhidrato"},
                    {"Nombre": "Viproxil", "Componente": "Pargeverina clorhidrato"},
                    {"Nombre": "Jarabe tos", "Componente": "Bromhexina hidrocloruro"},
                    {"Nombre": "Bisolvon", "Componente": "Bromhexina hidrocloruro"},
                    {"Nombre": "Antiacido", "Componente": "Carbonato de calcio"},
                    {"Nombre": "Nogastrol", "Componente": "Carbonato de calcio"},
                    {"Nombre": "Prolopa", "Componente": "L-dopa"},
                    {"Nombre": "Levofamil", "Componente": "L-dopa"}
                    ]

# Se almacenaran los codigos de medicamentos usados para que no se repitan.
codigos = []


# Esta funcion se ejecuta al inicio del programa y recopila los laboratorios.
# Si no existe archivo con los laboratorios, crea uno.
def inicia_labs():
    try:
        with open("labs.json", "r") as archivo:
            laboratorios = json.load(archivo)
            ls_labs = laboratorios["lab"]
            for item in ls_labs:
                if not item in labs:
                    labs.append(item)
    except FileNotFoundError:
        modifica_labs()


# Esta funcion modifica el archivo que contiene los nombres de laboratorios.
def modifica_labs():
    laboratorios = {}
    laboratorios["lab"] = labs
    with open("labs.json", "w") as archivo:
        json.dump(laboratorios, archivo, indent=4)


# Esta funcion crea el formato para los archivos de cada laboratorio.
def ficheros_lab(archivo=None):
    medicamentos = {}

    # Se comprueba la existencia del archivo. Si no existe un archivo para
    # ese laboratorio, crea uno con una seleccion de medicamentos genericos.
    ex = Path(archivo)
    if not ex.exists():
        medicamentos["Medicamentos"] = []

        # Se eligen 6 medicamentos genericos distintos al azar.
        while len(medicamentos["Medicamentos"]) < 6:
            eleccion = random.choice(medicamentos_gen)
            if not eleccion in medicamentos["Medicamentos"]:
                medicamentos["Medicamentos"].append(eleccion)

        # A cada medicamento se le asigna un codigo y un precio.
        for item in medicamentos["Medicamentos"]:
            # Se asigna un codigo que no este en uso.
            while True:
                codigo = random.randint(1000, 9999)
                if not codigo in codigos:
                    break
            item["Codigo"] = f"{codigo}"
            codigos.append(str(codigo))
            # Se le asigna un precio.
            precio = random.randint(300, 1500) * 10
            item["Precio"] = f"{precio}"

        # Se vuelca el contenido en un archivo.
        with open(archivo, "w") as fichero:
            json.dump(medicamentos, fichero, indent=4)

    # Si el archivo existe, la funcion se encarga
    # de recopilar los codigos que ya estan en uso.
    else:
        with open(archivo, "r") as fichero:
            medicamentos = json.load(fichero)
        for item in medicamentos["Medicamentos"]:
            x = item.get("Codigo")
            codigos.append(x)


# Esta funcion se encarga de seleccionar el nombre del archivo a abrir.
def elegir_archivo():
    # A cada laboratorio existente, se le asigna un numero,
    # de estos, el usuario debera elegir uno para abrir.
    for num, lab in enumerate(labs):
        print(f"< {num+1} > {lab}")

    while True:
        try:
            opcion = int(input("\nSelecciona el laboratorio deseado: "))
        except ValueError:
            print("Comando invalido.")
        if opcion > 0 and opcion <= len(labs):
            break
        else:
            print("Numero fuera de rango.")

    return opcion


# Esta funcion le da la estructura de diccionario
# a cada medicamento, al agregarlo manualmente.
def estructura_medicamento(nombre, componente, codigo, precio):
    med = {"Nombre": nombre,
           "Componente": componente,
           "Codigo": codigo,
           "Precio": precio
           }
    return med


# Esta funcion permite añadir un nuevo laboratorio a la lista.
def add_lab():
    print("\nOPCION 1: Agregar un laboratorio:\n")
    lab = input("Ingrese el nombre del laboratorio: ").capitalize()

    # Si el laboratorio a crear, no esta en la
    # lista de los ya existentes, lo agrega.
    if not lab in labs:
        labs.append(lab)
        modifica_labs()
        ficheros_lab(lab + ".json")
    else:
        print("Este lab ya existe en el registro.")


# Esta funcion agrega un medicamento al archivo del laboratorio elegido.
def agrega_medicamento():
    print("\nOPCION 2: Agregar medicamento:\n")
    time.sleep(zzz)
    opcion = elegir_archivo()

    # Se elige el nombre del nuevo medicamento.
    while True:
        nombre = input("\nIngrese el nombre del medicamento a agregar: ")
        if nombre == "" or nombre.startswith(" "):
            print("El medicamento debe tener un nombre valido.")
        else:
            nombre = nombre.capitalize()
            break

    # Se elige el componente activo del nuevo medicamento.
    while True:
        comp = input("\nIngrese el componente activo del medicamento: ")
        if comp == "" or comp.startswith(" "):
            print("Este campo debe tener un nombre valido.")
        else:
            comp = comp.capitalize()
            break

    # Se elige el codigo del nuevo medicamento.
    while True:
        try:
            codigo = int(input("\nIngrese el codigo del medicamento: "))
            if len(str(codigo)) != 4:
                print("El codigo debe ser un numero de 4 digitos.")
                continue
        except ValueError:
            print("El codigo debe ser un dato numerico de 4 digitos.")
            continue
        # El codigo tiene que ser uno no usado.
        if codigo in codigos:
            print("Ya existe un medicamento con este codigo.")
        else:
            codigo = str(codigo)
            codigos.append(codigo)
            break

    # Se elige el precio del nuevo medicamento.
    while True:
        try:
            precio = int(input("\nIngrese el precio del medicamento: "))
        except ValueError:
            print("Ingrese un valor numerico.")
            continue
        # No se le puede otorgar un precio menor a cero.
        if precio <= 0:
            print("El precio debe ser un valor positivo.")
        else:
            precio = str(precio)
            break

    # Se abre la lista de medicamentos para agregarle el nuevo.
    with open(labs[opcion-1] + ".json", "r") as fichero:
        lista_med = json.load(fichero)

    new_med = estructura_medicamento(nombre, comp, codigo, precio)
    lista_med["Medicamentos"].append(new_med)

    # Se reescribe el archivo, ahora con el nuevo medicamento.
    with open(labs[opcion-1] + ".json", "w") as fichero:
        json.dump(lista_med, fichero, indent=4)


# Esta funcion se encarga de consultar el precio de un medicamento especifico.
def consulta_precio():
    print("\nOPCION 3: Consultar precio de un medicamento:\n")
    time.sleep(zzz)
    opcion = elegir_archivo()
    print("\n")

    # Se abre el archivo del laboratorio seleccionado.
    with open(labs[opcion-1] + ".json", "r") as fichero:
        med_lab = json.load(fichero)
    med_lab = med_lab["Medicamentos"]

    # Se crea una lista auxiliar que contiene los nombres
    # de todos los medicamentos del laboratorio escogido.
    tmp = []
    for num, item in enumerate(med_lab):
        # Se crea una variable auxiliar para recoger el nombre del medicamento.
        aux = item.get("Nombre")

        # Esta linea imprime el numero asociado al nombre del medicamento.
        print(f"< {num + 1} > {aux}")
        tmp.append(f"{aux}")

    # Una vez los medicamentos son filtrados, el usuario
    # escoge el medicamento que necesita saber el precio.
    while True:
        try:
            cons = int(input("\nIngrese el medicamento que desea consultar: "))
        except ValueError:
            print("Comando invalido.")
            continue
        if cons <= 0 or cons > len(tmp):
            print("Numero fuera de rango.")
        else:
            break

    # Se imprime el precio.
    precio = med_lab[cons-1].get("Precio")
    print(f"\nEl medicamento {tmp[cons-1]} tiene un valor de {precio} pesos.\n")
    time.sleep(zzz)


# Esta funcion permite editar la informacion de un medicamento.
def editar_medicamento():
    print("\nOPCION 4: Editar un medicamento:\n")
    time.sleep(zzz)
    opcion = elegir_archivo()
    print("\n")

    # Se abre el archivo deseado y se conserva el texto.
    with open(labs[opcion-1] + ".json", "r") as fichero:
        med_lab = json.load(fichero)
    med_lab = med_lab["Medicamentos"]

    # Se crea una lista auxiliar para mostrar en pantalla los medicamentos
    # disponibles para ese laboratorio y el numero asociado a este.
    tmp = []
    for num, item in enumerate(med_lab):
        aux = item.get("Nombre")
        print(f"< {num + 1} > {aux}")
        tmp.append(f"{aux}")

    # El usuario elige el medicamento que quiere modificar.
    while True:
        try:
            edit = int(input("\nIngrese el medicamento que desea editar: "))
        except ValueError:
            print("Comando invalido.")
            continue
        if edit <= 0 or edit > len(tmp):
            print("Numero fuera de rango.")
        else:
            break

    # Se obtiene la info de cada medicamento.
    old_nombre = med_lab[edit-1].get("Nombre")
    old_comp = med_lab[edit-1].get("Componente")
    old_codigo = med_lab[edit-1].get("Codigo")
    old_precio = med_lab[edit-1].get("Precio")

    # Si se desea dejar un parametro sin modificar, en vez de escribirlo
    # nuevamente, el usuario puede dejar el campo en blanco.
    print("\nSi no desea modificar un parametro, deje esa casilla en blanco.")

    # Se ingresa un nuevo nombre.
    n = input(f"\nIngrese el nuevo nombre (Antes = {old_nombre}): ")
    if n == "":
        n = old_nombre
    else:
        n.capitalize()

    # Se ingresa un nuevo componente.
    a = input(f"\nIngrese el nuevo componente (Antes = {old_comp}): ")
    if a == "":
        a = old_comp
    else:
        a.capitalize()

    # Se ingresa un nuevo codigo.
    while True:
        try:
            c = input(f"\nIngrese el codigo (Antes = {old_codigo}): ")
            if c == "":
                c = old_codigo
                break
            else:
                c = int(c)
                # El codigo debe tener 4 digitos.
                if len(str(c)) != 4:
                    print("Entrada no valida. Ingrese un codigo de 4 digitos.")
                    continue

                # El codigo no puede estar en uso.
                if c in codigos and c != old_codigo:
                    print("Ese codigo ya esta en uso.")
                    continue
                x = codigos.index(old_codigo)
                # Se elimina el codigo antiguo y se agrega el nuevo.
                codigos.pop(x)
                codigos.append(c)
                break

        except ValueError:
            print("Codigo no valido. Ingrese un dato numerico de 4 digitos.")

    # Se ingresa un nuevo precio.
    while True:
        try:
            p = input(f"\nIngrese el precio (Antes = {old_precio}): ")
            if p == "":
                p = old_precio
                break
            p = int(p)
            if p <= 0:
                print("Precio no valido. Ingrese un numero mayor a cero.")
            else:
                break

        except ValueError:
            print("Codigo no valido. Ingrese un dato numerico de 4 digitos.")

    # Se modifican los valores del medicamento y se reescriben.
    med_lab[edit-1]["Nombre"] = f"{n}"
    med_lab[edit-1]["Componente"] = f"{a}"
    med_lab[edit-1]["Codigo"] = f"{c}"
    med_lab[edit-1]["Precio"] = f"{p}"
    with open(labs[opcion-1] + ".json", "w") as fichero:
        medicamentos = {}
        medicamentos["Medicamentos"] = med_lab
        json.dump(medicamentos, fichero, indent=4)
    print("\nMedicamento editado satisfactoriamente.\n")


# Esta funcion permite eliminar un medicamento de un laboratorio.
def eliminar_medicamento():
    print("\nOPCION 5: Eliminar medicamento:\n")
    time.sleep(zzz)
    opcion = elegir_archivo()
    print("\n")

    # Se abre el archivo del laboratorio escogido, y se recoge su contenido.
    with open(labs[opcion-1] + ".json", "r") as fichero:
        med_lab = json.load(fichero)
    med_lab = med_lab["Medicamentos"]

    # Se crea una lista temporal, para recoger el nombre de los
    # medicamentos y otorgarles un numero para que el usuario escoja.
    tmp = []
    for num, item in enumerate(med_lab):
        aux = item.get("Nombre")
        print(f"< {num + 1} > {aux}")
        tmp.append(f"{aux}")

    # Este ciclo gestiona la eliminacion del medicamento.
    while True:
        try:
            delete = int(input("\nIngrese el medicamento que desea eliminar: "))
        except ValueError:
            print("Comando invalido.")
            continue
        if delete <= 0 or delete > len(tmp):
            print("Numero fuera de rango.")
        else:
            med_lab.pop(delete-1)
            medicamentos = {}
            medicamentos["Medicamentos"] = med_lab
            # Se reescriben los datos.
            with open(labs[opcion-1] + ".json", "w") as fichero:
                json.dump(medicamentos, fichero, indent=4)
            print("\nMedicamento eliminado satisfactoriamente.")
            break


# Esta funcion se encarga de entregar toda la
# informacion relacionada al laboratorio escogido.
def info_labs():
    print("\nOPCION 6: Mostrar informacion de laboratorio:\n")
    time.sleep(zzz)
    opcion = elegir_archivo()
    print("\n")

    # Se abre el archivo y se recopilan sus datos.
    with open(labs[opcion-1] + ".json", "r") as fichero:
        meds = json.load(fichero)
    meds = meds["Medicamentos"]
    # Por cada medicamento, separa sus propiedades
    # y las imprime por pantalla en una linea.
    for diccionario in meds:
        n = diccionario.get("Nombre")
        a = diccionario.get("Componente")
        c = diccionario.get("Codigo")
        p = diccionario.get("Precio")
        print(f"Nombre: {n}, Comp.: {a}, Cod.: {c}, Precio: {p}")

    # No se vuelve inmediatamente al menu, para
    # que el usuario pueda leer los datos.
    try:
        input("\nPresione cualquier tecla para volver al menu: ")
    except:
        pass


# Esta funcion, busca todos los medicamentos con el mismo principio
# activo, los imprime por pantalla, e indica el mas barato de todos ellos.
def compara_precios():
    print("\nOPCION 7: Busqueda de precio mas bajo para un medicamento:")

    componentes = []
    info = []
    medicamentos = []

    # Por cada laboratorio, se almacenan todos los medicamentos
    # con su respectivo laboratorio y otros datos.
    for lab in labs:
        # Se abre el archivo del laboratorio a examinar.
        with open(lab + ".json", "r") as fichero:
            meds = json.load(fichero)
        meds = meds["Medicamentos"]
        # Se obtienen los datos de cada medicamento.
        for item in meds:
            precio = item.get("Precio")
            codigo = item.get("Codigo")
            comp = item.get("Componente")
            nombre = item.get("Nombre")

            # Si el medicamento examinado, es el primero en
            # mostrar un componente activo distinto, se agrega a
            # la lista que los contendra, una vez cada uno.
            if not comp in componentes:
                componentes.append(comp)

            # Finalmente, se agrega el medicamento
            # a la lista de todos los medicamentos.
            info.append([comp, nombre, lab, codigo, precio])
    print("\n")

    # A cada componente activo distinto, se le asocia un numero para
    # que el usuario consulte todos los medicamentos que lo contienen.
    for num, med in enumerate(componentes):
        print(f"< {num+1} > {med}")

    # Este ciclo gestiona la eleccion del usuario.
    while True:
        try:
            opcion = int(input("\nIngrese el componente deseado: "))
            if opcion <= 0 or opcion > len(componentes):
                print("Entrada fuera de rango.")
                continue
            else:
                break
        except ValueError:
            print("Entrada invalida.")

    # Cada medicamento que contenga el principio activo
    # especificado, es agregado a una lista para luego ser impreso.
    for tm in info:
        if tm[0] == componentes[opcion-1]:
            medicamentos.append(tm)

    # Se entregan los resultados.
    print(f"\nPara la busqueda con el componente {componentes[opcion-1]}")
    print("se obtuvieron los siguientes resultados:\n")
    # El primer elemento se designa como minimo por defecto, luego
    # si el programa encuentra un valor mas bajo, lo reemplaza.
    min = int(medicamentos[0][4])
    pb = medicamentos[0]
    for tm in medicamentos:
        if int(tm[4]) < min:
            min = int(tm[4])
            pb = tm
        print(f"{tm[1]}, precio {tm[4]}, laboratorio {tm[2]}, codigo {tm[3]}")

    print("\nLa alternativa de precio mas baja es: ")
    print(f"{pb[1]}, precio {pb[4]}, laboratorio {pb[2]}, codigo {pb[3]}")

    # No se vuelve inmediatamente al menu, para
    # que el usuario pueda leer los datos.
    try:
        input("\nPresione cualquier tecla para volver al menu: ")
    except:
        pass


def gestion_programa():
    while True:
        time.sleep(zzz)
        print("\nBienvenido al menu de la base de datos.\n")
        print("< 1 > Agregar un laboratorio")
        print("< 2 > Agregar un medicamento")
        print("< 3 > Consultar precio de un medicamento")
        print("< 4 > Editar un medicamento")
        print("< 5 > Eliminar un medicamento")
        print("< 6 > Mostrar informacion de laboratorio")
        print("< 7 > Busqueda de precio mas bajo para un medicamento")
        print("< 8 > Finalizar ejecucion")

        # Se elige una opcion y se manejan excepciones.
        try:
            opcion = int(input("\nQue accion desea realizar: "))
            time.sleep(zzz)
            if opcion == 1:
                add_lab()
            elif opcion == 2:
                agrega_medicamento()
            elif opcion == 3:
                consulta_precio()
            elif opcion == 4:
                editar_medicamento()
            elif opcion == 5:
                eliminar_medicamento()
            elif opcion == 6:
                info_labs()
            elif opcion == 7:
                compara_precios()
            elif opcion == 8:
                break
            else:
                print("La opcion elegida no es valida.")

        except ValueError:
            print("\nLa opcion elegida no es valida.")


# Se define la funcion principal del programa.
def main():
    # Se llama la funcion que cargara los laboratorios existentes.
    inicia_labs()
    # Por cada laboratorio, se abre (o se crea) un archivo.
    for lab in labs:
        ficheros_lab(f"{lab}.json")
    # Luego de que el programa se asegura de que los archivos existen,
    # se puede empezar a trabajar con las bases de datos.
    gestion_programa()


if __name__ == "__main__":
    main()
