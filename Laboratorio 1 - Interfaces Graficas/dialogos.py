import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Agregar_Entrada():
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("lab1.ui")

        self.v_add = builder.get_object("ventana_add")
        self.v_add.set_title("Añadir entrada")
        self.v_add.set_default_size(600, 400)

        self.btn_aceptar = builder.get_object("apply_add")
        #self.btn_aceptar.connect("clicked", self.aceptar_clicked)
        self.btn_cancel = builder.get_object("cancel_add")
        self.btn_cancel.connect("clicked", self.cancelar_clicked)

        self.entry_age = builder.get_object("entry_edad")
        self.entry_nak = builder.get_object("entry_nak")

        self.combobox_sex = builder.get_object("combo_sex")
        self.combobox_bp = builder.get_object("combo_bp")
        self.combobox_ch = builder.get_object("combo_colesterol")
        self.combobox_drug = builder.get_object("combo_drug")

        self.v_add.show_all()


    def cancelar_clicked(self, btn = None):
        self.v_add.destroy()


class Error_Agregar():
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("lab1.ui")

        self.v_error = builder.get_object("agregar_error")

        self.v_error.show_all()


class Confirmar_Delete():
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("lab1.ui")

        self.v_confirmar = builder.get_object("confirmar_eliminacion")

        self.btn_cancel = builder.get_object("cancelar_delete")
        self.btn_aceptar = builder.get_object("aceptar_delete")

        self.v_confirmar.show_all()


class Resumen_Entradas():
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("lab1.ui")

        self.v_resumen = builder.get_object("resumen")
        self.v_resumen.set_title("Resumen")
        self.v_resumen.set_resizable(False)

        self.promedio_edad = builder.get_object("edad_promedio")
        self.cantidad_mujeres = builder.get_object("numero_fem")
        self.cantidad_hombres = builder.get_object("numero_masc")
        self.cantidad_bp_low = builder.get_object("numero_bp_low")
        self.cantidad_bp_normal = builder.get_object("numero_bp_normal")
        self.cantidad_bp_high = builder.get_object("numero_bp_high")
        self.cantidad_ch_normal = builder.get_object("numero_ch_normal")
        self.cantidad_ch_high = builder.get_object("numero_ch_high")
        self.promedio_nak = builder.get_object("promedio_nak")
        self.cantidad_drug_A = builder.get_object("numero_drugA")
        self.cantidad_drug_B = builder.get_object("numero_drugB")
        self.cantidad_drug_C = builder.get_object("numero_drugC")
        self.cantidad_drug_X = builder.get_object("numero_drugX")
        self.cantidad_drug_Y = builder.get_object("numero_drugY")

        self.btn_cerrar = builder.get_object("cerrar_resumen")
        self.btn_cerrar.connect("clicked", self.cerrar)

        self.v_resumen.set_default_size(600, 400)

        self.v_resumen.show_all()

    def cerrar(self, btn = None):
        self.v_resumen.destroy()
