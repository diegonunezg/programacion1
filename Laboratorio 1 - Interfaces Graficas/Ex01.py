import pandas
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from dialogos import Agregar_Entrada, Error_Agregar, Confirmar_Delete, Resumen_Entradas


def add_to_csv(agregar = None):
    """ Esta funcion agrega lineas al archivo drug200.csv """
    with open("./drug200.csv", "a") as archivo:
        archivo.write(agregar)


class Ventana_Principal():
    """ Esta clase define a la ventana principal del programa """
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("lab1.ui")

        # Instancia la ventana. Se le da un titulo y una resolucion por defecto.
        principal = builder.get_object("v_principal")
        principal.set_default_size(1200, 600)
        principal.set_title("Drugs")
        principal.connect("destroy", Gtk.main_quit)

        # Se regoge el widget del tipo tree_view
        self.tree_view = builder.get_object("drugs_summary")

        # Se recoge el botón de eliminar, y se conecta con su acción
        self.btn_eliminar = builder.get_object("eliminar_entrada")
        self.btn_eliminar.connect("clicked", self.confirmar_eliminacion)

        # Se recoge el botón de resumen y se conecta con su acción
        self.btn_resumen = builder.get_object("resumen_entrada")
        self.btn_resumen.connect("clicked", self.resumen)

        # Se recoge el boton añadir y se conecta con su acción
        self.btn_add = builder.get_object("agregar_entrada")
        self.btn_add.connect("clicked", self.add_entry)

        # Se carga el archivo al tree_view
        self.create_view("./drug200.csv")

        # Se muestra la ventana principal
        principal.show_all()


    def confirmar_eliminacion(self, btn = None):
        """ Esta función levanta una ventana de pregunta """
        # La ventana esta definida por la clase importada Confirmar_Delete
        dialogo = Confirmar_Delete()
        v_confirmar_delete = dialogo.v_confirmar

        # Se espera una respuesta de un Gtk.Button
        response = v_confirmar_delete.run()

        # Si la respuesta es ACCEPT, confirma la eliminación de la entrada
        if response == Gtk.ResponseType.ACCEPT:
            self.delete_entry()

        # Finalmente, se destruye la ventana de pregunta
        v_confirmar_delete.destroy()


    def delete_entry(self):
        """ Esta funcion se encarga de eliminar una entrada del archivo """
        model, it = self.tree_view.get_selection().get_selected()
        # Si no hay nada seleccionado, no hace nada
        if model is None or it is None:
            return

        # La lista selección almacenará los datos a eliminar de la entrada
        seleccion = []

        # Las entradas se componen de 6 columnas
        for i in range(6):
            seleccion.append(model.get_value(it, i))

        # Se crea la linea a borrar en el mismo formato csv
        linea = ",".join(seleccion) + "\n"

        # Se abre el archivo y se copia el contenido
        with open("./drug200.csv", "r") as archivo:
            contenido = archivo.readlines()

        # La lista new_content guardará todas las lineas menos la eliminada
        new_content = []
        for line in contenido:
            # Si la linea actual no corresponde con la deseada la guarda
            if line != linea:
                new_content.append(line)

        # Finalmente se guarda el Archivo
        with open("./drug200.csv", "w") as archivo:
            for item in new_content:
                archivo.write(item)

        # Se actualiza el tree_view
        self.create_view("./drug200.csv")


    def resumen(self, btn = None):
        """ Esta función levanta una ventana con un resumen de las entradas """
        # La ventana esta definida por la clase importada Resumen_Entradas
        dialogo = Resumen_Entradas()
        ventana_resumen = dialogo.v_resumen

        # A continuación se recogen todos los labels a rellenar con información
        label_edad = dialogo.promedio_edad
        label_mujeres = dialogo.cantidad_mujeres
        label_hombres = dialogo.cantidad_hombres
        label_bp_low = dialogo.cantidad_bp_low
        label_bp_normal = dialogo.cantidad_bp_normal
        label_bp_high = dialogo.cantidad_bp_high
        label_ch_normal = dialogo.cantidad_ch_normal
        label_ch_high = dialogo.cantidad_ch_high
        label_nak = dialogo.promedio_nak
        label_drugA = dialogo.cantidad_drug_A
        label_drugB = dialogo.cantidad_drug_B
        label_drugC = dialogo.cantidad_drug_C
        label_drugX = dialogo.cantidad_drug_X
        label_drugY = dialogo.cantidad_drug_Y

        # Se calcula el promedio de la edad y se le hace un set_text al label
        sum_edad = 0
        for item in self.data.Age:
            sum_edad += item
        prom_edad = int(sum_edad/len(self.data.Age))
        label_edad.set_label(str(prom_edad))

        # Se cuentan la cantidad de personas por sexo
        # y se le hace un set al texto del label respectivo.
        mujeres = 0
        hombres = 0
        for item in self.data.Sex:
            if item == "F":
                mujeres += 1
            else:
                hombres+= 1
        label_mujeres.set_label(str(mujeres))
        label_hombres.set_label(str(hombres))

        # Se cuentan la cantidad de personas por cada tipo de
        # presión y se le hace un set al texto del label respectivo
        bp_low = []
        bp_normal = []
        bp_high = []
        for item in self.data.BP:
            if item == "LOW":
                bp_low.append(item)
            elif item == "NORMAL":
                bp_normal.append(item)
            else:
                bp_high.append(item)
        label_bp_low.set_label(str(len(bp_low)))
        label_bp_normal.set_label(str(len(bp_normal)))
        label_bp_high.set_label(str(len(bp_high)))

        # Se cuentan la cantidad de personas por cada tipo de
        # colesterol y se le hace un set al texto del label respectivo
        ch_normal = []
        ch_high = []
        for item in self.data.Cholesterol:
            if item == "NORMAL":
                ch_normal.append(item)
            else:
                ch_high.append(item)
        label_ch_normal.set_label(str(len(ch_normal)))
        label_ch_high.set_label(str(len(ch_high)))

        # Se calcula el promedio de la razón Na/K y se le
        # hace un set al texto del label respectivo
        sum_nak = 0
        for item in self.data.Na_to_K:
            sum_nak += item
        prom_nak = sum_edad/len(self.data.Na_to_K)
        label_nak.set_label("{0:.3f}".format(prom_nak))

        # Se cuentan la cantidad de personas que consumen cada tipo de
        # medicamento y se le hace un set al texto del label respectivo
        c_drugA = []
        c_drugB = []
        c_drugC = []
        c_drugX = []
        c_drugY = []
        for item in self.data.Drug:
            if item == "drugA":
                c_drugA.append(item)
            elif item == "drugB":
                c_drugB.append(item)
            elif item == "drugC":
                c_drugC.append(item)
            elif item == "drugX":
                c_drugX.append(item)
            else:
                c_drugY.append(item)
        label_drugA.set_label(str(len(c_drugA)))
        label_drugB.set_label(str(len(c_drugB)))
        label_drugC.set_label(str(len(c_drugC)))
        label_drugX.set_label(str(len(c_drugX)))
        label_drugY.set_label(str(len(c_drugY)))


    def aceptar_clicked(self, btn = None):
        """ Esta funcion se encarga de que al momento de agregar una
        entrada al tree_view, todos los datos esten bien ingresados """

        # La variable errores trackea la cantidad de los mismos
        errores = 0

        # La edad y la razón Na/K se obtienen de un Gtk.Entry
        age = self.age.get_text()
        nak = self.nak.get_text()

        # Si la edad no es un valor numérico, aumenta 1 la cantidad de errores
        try:
            age = int(float(age))
        except ValueError:
            errores += 1

        # Si la razón no es un valor numérico, aumenta 1 la cantidad de errores
        try:
            nak = float(nak)
        except ValueError:
            errores += 1

        # Si los combobox asociados a cada dato tienen seleccionados la opción
        # por defecto "- Seleccionar -" aumenta en 1 la cantidad de errores
        if self.combo_sex.get_active() == 0:
            errores += 1
        if self.combo_bp.get_active() == 0:
            errores += 1
        if self.combo_ch.get_active() == 0:
            errores += 1
        if self.combo_drug.get_active() == 0:
            errores += 1

        # Si no hubo ningún error, se guarda la información proporcionada
        if errores == 0:
            # Esta lista guardará cada dato para luego darles formato csv
            texto = []

            # Se obtiene la edad
            texto.append(str(age))

            # Se obtiene lo seleccionado en las combobox
            sex_iter = self.combo_sex.get_active_iter()
            sex_model = self.combo_sex.get_model()
            texto.append(sex_model[sex_iter][0])

            bp_iter = self.combo_bp.get_active_iter()
            bp_model = self.combo_bp.get_model()
            texto.append(bp_model[bp_iter][0])

            ch_iter = self.combo_ch.get_active_iter()
            ch_model = self.combo_ch.get_model()
            texto.append(ch_model[ch_iter][0])

            texto.append(str(nak))

            drug_iter = self.combo_drug.get_active_iter()
            drug_model = self.combo_drug.get_model()
            texto.append(drug_model[drug_iter][0])

            # Se formatea el texto final
            final_text = ",".join(texto) + "\n"

            # Se agrega al archivo csv
            add_to_csv(final_text)
            # Se actualiza el tree_view
            self.create_view("./drug200.csv")
            # Una vez terminada su tarea, la ventana se destruye
            self.ventana_add.destroy()

        # Si se presentaron errores, levanta una ventana de advertencia
        else:
            self.error_al_agregar()


    def error_al_agregar(self):
        """ Esta función levanta la ventana de
        advertencia de mal ingreso de datos """

        # La ventana esta definida por la clase importada Error_Agregar
        dialogo = Error_Agregar()
        ventana_error = dialogo.v_error

        # Se espera una respuesta de la ventana
        response = ventana_error.run()

        # Si la respuesta es de tipo CLOSE, destruye la ventana
        if response == Gtk.ResponseType.CLOSE:
            ventana_error.destroy()


    def add_entry(self, btn = None):
        """ Esta funcion esta asociada al boton añadir de la ventana principal,
        levanta un dialogo donde se puede añadir una entrada al csv"""

        # La ventana esta definida por la clase importada Agregar_Entrada
        dialogo = Agregar_Entrada()
        self.ventana_add = dialogo.v_add

        # Se recoge el boton aceptar de la ventana y se conecta con su evento
        btn_aceptar = dialogo.btn_aceptar
        btn_aceptar.connect("clicked", self.aceptar_clicked)

        # Se recogen las entry y los combobox para ingresar los datos deseados
        self.age = dialogo.entry_age
        self.nak = dialogo.entry_nak
        self.combo_sex = dialogo.combobox_sex
        self.combo_bp = dialogo.combobox_bp
        self.combo_ch = dialogo.combobox_ch
        self.combo_drug = dialogo.combobox_drug

        render = Gtk.CellRendererText()

        """ Combobox_text dedicada al parametro Sex """
        self.combo_sex.pack_start(render, True)
        self.combo_sex.add_attribute(render, "text", 0)
        self.modelo = Gtk.ListStore(str)
        self.combo_sex.set_model(self.modelo)

        self.modelo.append(["- Seleccionar -"])
        for item in self.data.Sex.unique():
            self.modelo.append([item])
        self.combo_sex.set_active(0)

        """ Combobox dedicada al parametro BP """
        self.combo_bp.pack_start(render, True)
        self.combo_bp.add_attribute(render, "text", 0)
        self.modelo = Gtk.ListStore(str)
        self.combo_bp.set_model(self.modelo)

        self.modelo.append(["- Seleccionar -"])
        for item in self.data.BP.unique():
            self.modelo.append([item])
        self.combo_bp.set_active(0)

        """ Combobox dedicada al parametro Cholesterol """
        self.combo_ch.pack_start(render, True)
        self.combo_ch.add_attribute(render, "text", 0)
        self.modelo = Gtk.ListStore(str)
        self.combo_ch.set_model(self.modelo)

        self.modelo.append(["- Seleccionar -"])
        for item in self.data.Cholesterol.unique():
            self.modelo.append([item])
        self.combo_ch.set_active(0)

        """ Combobox dedicada al parametro Drug """
        self.combo_drug.pack_start(render, True)
        self.combo_drug.add_attribute(render, "text", 0)
        self.modelo = Gtk.ListStore(str)
        self.combo_drug.set_model(self.modelo)

        self.modelo.append(["- Seleccionar -"])
        for item in self.data.Drug.unique():
            self.modelo.append([item])
        self.combo_drug.set_active(0)


    def create_view(self, path_file):
        """ Se crea la estructura de columnas en el tree_view """

        # Se lee el archivo csv
        self.data = pandas.read_csv(path_file)

        # Se eliminan columnas
        if self.tree_view.get_columns():
            for column in self.tree_view.get_columns():
                self.tree_view.remove_column(column)

        # Se genera el modelo de columnas del tree_view
        largo_columnas = len(self.data.columns)
        modelo = Gtk.ListStore(*(largo_columnas * [str]))
        self.tree_view.set_model(model=modelo)

        cell = Gtk.CellRendererText()

        # A cada columna se le agrega las celdas
        for item in range(len(self.data.columns)):
            column = Gtk.TreeViewColumn(self.data.columns[item],
                                        cell,
                                        text=item)
            self.tree_view.append_column(column)
            column.set_sort_column_id(item)

        # Se generan las lineas del tree_view
        for item in self.data.values:
            line = [str(x) for x in item]
            modelo.append(line)


if __name__ == "__main__":
    Ventana_Principal()
    Gtk.main()
