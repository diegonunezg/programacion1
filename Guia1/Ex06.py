"""
Ejercicio 6

Un estacionamiento requiere determinar el cobro que debe aplicar a las
personas que lo utilizan. Considere que el cobro es con base en las
horas que lo disponen y que las fracciones de hora se toman como
completas. Cree un programa que realice el cobro del estacionamiento.

"""
# Variables
valor_hora = 325
horas = 0
tmp = 0

# Este ciclo se ejecuta siempre que la cantidad de tiempo ingresada sea invalida.
while tmp == 0:
	minutos = int(input("Ingrese la cantidad de minutos que estuvo estacionado: "))
	if minutos >= 0:
		break
	else:
		print("La cantidad de tiempo ingresado no es valida. Intente de nuevo.")

# Este ciclo calcula las horas a cobrar, teniendo en cuenta las fracciones de hora.
while minutos > 0:
	minutos -= 60
	horas += 1

# Se entrega el valor final a pagar.
print("El precio a pagar por ", horas, "horas es de", horas * valor_hora)

