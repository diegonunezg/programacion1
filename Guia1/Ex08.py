"""
Ejercicio 8

Se requiere obtener el area de la figura en la forma A de la siguiente imagen.
Para resolver este problema se puede partir de que esta formada por tres figuras:
dos triangulos rectangulos, con H como hipotenusa y R como uno de los catetos,
que tambien es el radio de la otra figura, una semicircunferencia que forma
la parte circular (ver forma B).

"""

# Se importa la constante pi para el calculo del area de la circunferencia.
from math import pi

# Se pide ingresar el radio y la hipotenusa.
R = int(input("Ingrese el radio de la semicircunferencia: "))
H = int(input("Ingrese la hipotenusa de los triangulos: "))

# Esta expresion resulta de despejar un cateto en el teorema de pitagoras.
cateto = (H**2 - R**2)**(1/2.0)

# Se calculan las areas para las figuras.
area_triangulos = R * cateto
area_semicircunferencia = (pi * (R**2)) / 2.0

# Se entrega el resultado.
print("El area de la figura completa es de ", area_triangulos + area_semicircunferencia)

