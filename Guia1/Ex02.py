"""
Ejercicio 2

Inicialice una variable en None y si la palabra ingresada es
“Hola” debe imprimir Chao, caso contrario debe decir, no
entiendo tu mensaje e imprimir el valor de la variable.

"""

# Se inicializa una variable con None y luego alamcena un mensaje.
palabra = None
palabra = input("Ingresa un mensaje: ")

# La respuesta al mensaje del usuario depende del contenido ingresado.
if palabra == "Hola":
    print("Chao")
else:
    print("No entiendo tu mensaje:", palabra)

