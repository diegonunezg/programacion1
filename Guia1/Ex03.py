"""
Ejercicio 3

Una multitienda tiene una promocion: a todos los trajes que tienen
un precio superior a $10000 se les aplicara un descuento de 15 %,
a todos los demas se les aplicara solo 8 %. Realice un programa para
determinar el precio final que debe pagar una persona por comprar un
traje y de cuanto es el descuento que obtendra.
Pida por lo menos ingresar 3 elementos.

"""

# Variables
trajes_comprados = 0
total_pagar = 0
precio = 0
precio_pagar = 0
tmp = 0

# Este ciclo siempre se ejecuta. 
while True:
    print("< Numero 1 > Para comprar un traje")
    print("< Otro numero > Para finalizar ")
    tmp = int(input("Elija una opcion: "))
    
    if tmp == 1:
        pass
    # Si la cantidad de trajes es mayor o igual a 3 se sale del ciclo.
    elif trajes_comprados >= 3:
        break
    # Sino, se pide comprar como minimo 3 trajes.
    else:
    	print("Debe comprar como minimo 3 trajes")
    	continue
    
    # Si se elije comprar otro traje, se pide el precio.
    precio = int(input("Ingrese el precio del traje a comprar: "))
    # Y dependiendo del precio se hace un descuento.
    if precio > 10000:
    	precio_pagar = precio*0.85
    else:
    	precio_pagar = precio*0.92
	
    total_pagar += precio_pagar
    trajes_comprados += 1
    print("El precio final de este traje es de ", precio_pagar)

# Se entrega la cantidad de trajes comprados y el precio total con descuentos.
print("La compra de los ", trajes_comprados, "tiene un valor de ", total_pagar)

