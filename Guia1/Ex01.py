"""
Ejercicio 1 

Realice un programa en Python para determinar cuanto se debe pagar
por equis cantidad de lapices considerando que si son 1000 o mas el
costo es de 85 pesos; de lo contrario, el precio es de 90 pesos.

"""

# Se le da un valor de tipo entero a la variable lapices.
lapices = int(input("Ingresa la cantidad de lapices a comprar: "))

# El precio por lapiz cambia en funcion de cuantos son comprados.
if lapices < 1000:
    print("El valor total de la compra es de ", lapices*90)
else:
    print("El valor total de la compra es de ", lapices*85)

