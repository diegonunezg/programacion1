"""
Ejercicio 9

Se requiere obtener la distancia entre dos puntos en el plano cartesiano,
tal y como se muestra en la figura. Para resolver este problema es necesario
conocer las coordenadas de cada punto (X, Y), y con esto poder obtener el
cateto de abscisas (X1 y X2) y el de ordenadas (Y1 e Y2), y mediante estos
valores obtener la distancia entre P1 y P2, utilizando el teorema de Pitagoras.

"""

# Se ingresan las coordenadas de los puntos.
x1 = int(input("Ingrese la coordenada x del primer punto: "))
y1 = int(input("Ingrese la coordenada y del primer punto: "))
x2 = int(input("Ingrese la coordenada x del segundo punto: "))
y2 = int(input("Ingrese la coordenada y del segundo punto: "))

# Se calcula la distancia entre coordenadas x e y.
cateto_x = x2 - x1
cateto_y = y2 - y1

# Si el resultado es negativo se le cambia el signo. Las distancias son siempre positivas.
if cateto_x < 0:
	cateto_x *= -1
if cateto_y < 0:
	cateto_y *= -1

# Se calcula la distancia entre los puntos utilizando el teorema de pitagoras.
hipotenusa = (cateto_x**2 + cateto_y**2)**(1/2.0)

# Se entrega el resultado.
print("La distancia entre los dos puntos es de ", hipotenusa)

