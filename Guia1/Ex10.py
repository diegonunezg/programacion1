"""
Ejercicio 10

Una empresa constructora vende terrenos con la forma A de la figura.
Se requiere crear un programa para obtener el area respectiva de un
terreno de medidas de cualquier valor. Para resolver este problema
se debe identificar que la forma A esta compuesta por dos figuras:
un triangulo de base B y de altura (A - C); y por otro lado, un rectangulo
que tiene base B y altura C y la suma de ambas areas es el resultado final.

"""

# Este ciclo siempre se ejecuta y se asegura de que las distancias sean validas.
while True:
	A = int(input("Ingrese la medida del lado A: "))
	B = int(input("Ingrese la medida del lado B: "))
	C = int(input("Ingrese la medida del lado C: "))
	
	# Segun la figura, el lado A debe ser mayor que C.
	if C > A:
		print("""
		Medidas incorrectas geometricamente. 
		Intente de nuevo. (A debe ser mayor que C)
		
		""")
	# Por otro lado las distancias no pueden ser menores o iguales a cero. 
	elif A <= 0 or B <= 0 or C <= 0:
		print("Una de las medidas es invalida. (Las dimensiones deben ser mayores a cero)")
	else:
		break

# Se calcula el area de las 2 figuras.
area_rectangulo = B * C
area_triangulo = ((A - C) * B) / 2.0

# Se entrega el area total del terreno.
print("El area total del terreno es de ", area_rectangulo + area_triangulo)

