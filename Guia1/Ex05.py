"""
Ejercicio 5

Una empresa que contrata personal requiere determinar la edad de cada
una de las personas que solicitan trabajo, pero cuando se les realiza
la entrevista solo se les pregunta el año en que nacieron.
Tambien se requiere el promedio de edad de las personas contratadas.

"""

# Variables
contratados = 0
total_edades = 0
ano_actual = 2021
ano = 0
edad = 0
tmp = 0

# Este ciclo siempre se ejecuta.
while True:
    print("< Numero 1 > Para contratar mas personal")
    print("< Otro numero > Para finalizar ")
    tmp = int(input("Elija una opcion: "))
    # Cuando se desee dejar de contratar, se escapa del ciclo.
    if tmp == 1:
        pass
    else:
        break
    # Se calcula la edad de la persona contratada.
    ano = int(input("Ingrese el año de nacimiento de la persona a contratar: "))
    edad = ano_actual - ano
    total_edades += edad
    contratados += 1
    print("La edad de esta persona es ", edad)

# Se entrega la informacion recopilada al final de la ejecucion.
if contratados == 0:
    print("No se contrato a nadie")
else:
    print("El promedio de edades de los ", contratados, " contratados es de ", total_edades/contratados)

