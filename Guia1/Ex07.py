"""
Ejercicio 7

“El buen gordito” ofrece hamburguesas sencillas, dobles y triples,
las cuales tienen un costo de $2000, $2500 y $3000 respectivamente.
La empresa acepta tarjetas de credito con un cargo de 5 % sobre la
compra. Suponiendo que los clientes adquieren solo un tipo de hamburguesa,
realice un algoritmo para determinar cuanto debe pagar una persona por N hamburguesas.

"""

# Variable que define el tipo de hamburguesa.
tipo = 0

# Este ciclo siempre se ejecuta hasta que se elija una opcion valida.
while True:
	print("""Bienvenido a El buen gordito. Tenemos estas hamburguesas disponibles:
	
		  < 1 > Sencilla  $ 2000
		  < 2 > Doble     $ 2500
		  < 3 > Triple    $ 3000
		  
		  """)
	tipo = int(input("Ingrese la hamburguesa que desea comprar: "))
	if tipo < 1 or tipo > 3:
		print("El codigo ingresado es invalido. Intente de nuevo.")
	# Cuando la opcion elegida es validada, se escapa del ciclo.
	else:
		break

# Se ingresa la cantidad de hamburguesas a comprar.
N = int(input("Ingrese cuantas hamburguesas desea comprar: "))

# Se calcula el precio a pagar.
if tipo == 1:
	precio_total = N * 2000
elif tipo == 2:
	precio_total = N * 2500
else:
	precio_total = N * 3000

# Si se paga con tarjeta de credito, se aplica un cargo extra.
credito = int(input("¿Paga con tarjeta de credito? < 1 > Si  < Otro > No : "))

if credito == 1:
	print("El precio a pagar por ", N," hamburguesas es de ", precio_total * 1.05)
else:
	print("El precio a pagar por ", N," hamburguesas es de ", precio_total)

