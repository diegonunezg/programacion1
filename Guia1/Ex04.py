"""
Ejercicio 4

Se requiere un programa para determinar cuanto ahorrara una persona en
un año, si al final de cada mes deposita variables cantidades de
dinero; ademas, se requiere saber cuanto lleva ahorrado cada mes.

"""

# Variables que guardaran cantidades de dinero.
mensualidad = None
total = 0

# Este ciclo recorre los (12) meses del año y gestiona un deposito por mes.
for deposito in range(0,12):
    print ("Mes ", deposito + 1)
    mensualidad = int(input("Ingrese el deposito del mes: "))
    total += mensualidad
    print("Llevas ahorrado ", total, " hasta la fecha")

print("En un año ahorraste: ", total)

