# Se importa la librería Gobject Instrospection (gi)
import gi
# Se selecciona que la versión de GTK a trabajar (3.0)
gi.require_version("Gtk", "3.0")
# Se importa Gtk
from gi.repository import Gtk


def convert_int(value = None):
    """ Esta función retorna un dato de tipo entero a partir de un valor. """
    if value.isdigit():
        return int(value)
    else:
        return len(value)


# Se define la clase Aplicacion, relacionada con la ventana principal.
class Aplicacion:
    def __init__(self):
        builder = Gtk.Builder()
        # Se llama el archivo asociado a la aplicación.
        builder.add_from_file("ex1.ui")
        # Se rescata el la ventana principal.
        ventana = builder.get_object("Principal")
        # Se maximiza la ventana.
        ventana.maximize()
        # Se le otorga el título "sumatorias".
        ventana.set_title("Sumatorias")
        # La ejecución de la aplicación termina al destruir la ventana.
        ventana.connect("destroy", Gtk.main_quit)
        ventana.show_all()

        # Se rescantan 2 botones con id "reset" y "ok"
        self.reset = builder.get_object("reset")
        self.ok = builder.get_object("ok")

        # A los botones, se les asocia eventos "clicked".
        self.reset.connect("clicked", self.boton_reset)
        self.ok.connect("clicked", self.boton_ok)

        # Se rescatan entradas de texto que corresponderán a los sumandos.
        # Y se les asocia un evento "activate" al apretar la tecla enter.
        self.sum1 = builder.get_object("sum1")
        self.sum1.connect("activate", self.enter_suma)

        self.sum2 = builder.get_object("sum2")
        self.sum2.connect("activate", self.enter_suma)

        # Se rescata el label con id "res", que mostrará el resultado de la suma
        self.res = builder.get_object("res")


    def enter_suma(self, enter = None):
        """ Este método recopila los sumandos y realiza la operación """

        # Se rescatan los valores ingresados por el usuario para sumarlos.
        sumando1 = self.sum1.get_text()
        sumando2 = self.sum2.get_text()

        # Según el tipo de dato, se toma el largo de la cadena o el numero.
        sumando1 = convert_int(sumando1)
        sumando2 = convert_int(sumando2)

        # Se muestra el resultado de la suma en el label asociado al resultado.
        self.res.set_text(str(sumando1 + sumando2))

        return sumando1 + sumando2


    def boton_reset(self, btn = None):
        """ Este método reestablece las entradas de texto, dejandolas vacías """

        # Presionar el boton reset, limpia las entradas de texto y el resultado.
        self.res.set_text("")
        self.sum1.set_text("")
        self.sum2.set_text("")


    def boton_ok(self, btn = None):
        """
        Este método es desencadenado cuando el botón Aceptar de la
        ventana principal emite una señal 'clicked'. Cuando esto ocurre,
        se llama a la clase Dialogo y se abre una ventana de dialogo que
        muestra los resultados de la sumatoria.
        """

        # Se llama a la clase Dialogo.
        ventana_dialogo = Dialogo()

        # Se asigna los valores correspondientes a labels del dialogo emergente.
        ventana_dialogo.res_dialog.set_text(str(self.enter_suma()))
        ventana_dialogo.sum1_dialog.set_text(self.sum1.get_text())
        ventana_dialogo.sum2_dialog.set_text(self.sum2.get_text())

        # El método run() espera una respuesta al dialogo por parte del usuario.
        response = ventana_dialogo.dialogo.run()

        # Dependiedo de la respuesta dada, se llevan a cabo distintas acciones.
        if response == Gtk.ResponseType.CANCEL:
            pass
        elif response == Gtk.ResponseType.OK:
            # Al recibir OK, los valores de la ventana principal se reestablecen
            self.res.set_text("")

            # Dependiendo si lo ingresado era numérico, se resetea con 0 o ""
            if self.sum1.get_text().isdigit():
                self.sum1.set_text(str(0))
            else:
                self.sum1.set_text("")

            if self.sum2.get_text().isdigit():
                self.sum2.set_text(str(0))
            else:
                self.sum2.set_text("")

        elif response == Gtk.ResponseType.DELETE_EVENT:
            ventana_dialogo.dialogo.destroy()

        # Al finalizar, la ventana se destruye.
        ventana_dialogo.dialogo.destroy()


# Se define la clase Dialogo, relacionada a la ventana de dialogo emergente.
class Dialogo:
    def __init__(self):
        builder = Gtk.Builder()
        # Se llama el archivo asociado a la aplicación.
        builder.add_from_file("ex1.ui")

        # Se rescata la ventana de dialogo.
        self.dialogo = builder.get_object("dialogo")

        # Se rescatan los labels para luego asignarles un valor.
        self.sum1_dialog = builder.get_object("sum1_dialog")
        self.sum2_dialog = builder.get_object("sum2_dialog")
        self.res_dialog = builder.get_object("res_dialog")

        # Se muestra todo el contenido de la ventana.
        self.dialogo.show_all()


if __name__ == "__main__":
    # El llamado a la clase instancia la ventana y sus objetos.
    Aplicacion()
    # Gtk.main() ejecuta la aplicación hasta que recbe una señal Gtk.main_quit
    Gtk.main()
